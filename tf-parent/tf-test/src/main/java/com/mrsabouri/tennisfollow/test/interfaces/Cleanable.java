package com.mrsabouri.tennisfollow.test.interfaces;

public interface Cleanable {
  void clean();
}
