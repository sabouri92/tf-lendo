package com.mrsabouri.tennisfollow.metrics.config;

import io.micrometer.core.instrument.Meter;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.config.MeterFilter;
import io.micrometer.core.instrument.distribution.DistributionStatisticConfig;
import io.micrometer.core.lang.NonNull;
import org.springframework.boot.actuate.autoconfigure.metrics.MeterRegistryCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;

@Configuration
public class TennisFollowMetricsAutoConfiguration {

  @Bean
  @Profile("!test")
  public MeterRegistryCustomizer<MeterRegistry> meterRegistryCustomizer(Environment environment) {
    return registry ->
        registry.config()
            .meterFilter(new MeterFilter() {
              @Override
              public DistributionStatisticConfig configure(Meter.Id id, @NonNull DistributionStatisticConfig config) {
                if (id.getName().contains("http.server.requests")) {
                  return DistributionStatisticConfig.builder()
                      .percentiles(0.95, 0.99, 0.999)
                      .percentilesHistogram(true)
                      .build()
                      .merge(config);
                }
                return config;
              }
            })
            .commonTags(
                "hostname", environment.getProperty("hostname"),
                "application", environment.getProperty("spring.application.name"),
                "cluster-name", environment.getProperty("cluster.name")
            );
  }

}
