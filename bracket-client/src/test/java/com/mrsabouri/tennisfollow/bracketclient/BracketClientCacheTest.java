package com.mrsabouri.tennisfollow.bracketclient;

import com.mrsabouri.tennisfollow.bracketclient.impl.BracketClientImpl;
import com.mrsabouri.tennisfollow.bracketclient.model.Match;
import com.mrsabouri.tennisfollow.bracketclient.model.MatchByPlayersRequest;
import com.mrsabouri.tennisfollow.bracketclient.model.Team;
import com.mrsabouri.tennisfollow.bracketclient.model.Tournament;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.Mockito.*;

@Import(TestConfiguration.class)
@RunWith(SpringRunner.class)
public class BracketClientCacheTest {
  @MockBean
  private RestTemplate restTemplate;

  @Autowired
  private BracketClient bracketClient;

  private Team undeterminedTeamMock;
  private Team determinedTeamMock;

  @Before
  public void setup() {
    undeterminedTeamMock = mock(Team.class);
    determinedTeamMock = mock(Team.class);

    when(undeterminedTeamMock.isPlayersDetermined()).thenReturn(false);
    when(determinedTeamMock.isPlayersDetermined()).thenReturn(true);
  }

  @Test
  public void cachesTournamentByUrl() {
    Tournament tournament = new Tournament();

    when(restTemplate.getForObject(any(), any(), anyMap())).thenReturn(tournament);

    bracketClient.getTournamentByUrl("some-url-to-cache");
    bracketClient.getTournamentByUrl("some-url-to-cache");

    verify(restTemplate, times(1)).getForObject(any(), any(), anyMap());
    verifyNoMoreInteractions(restTemplate);
  }

  @Test
  public void noCacheTournamentIdDifferentUrls() {
    Tournament tournament = new Tournament();

    when(restTemplate.getForObject(any(), any(), anyMap())).thenReturn(tournament);

    bracketClient.getTournamentByUrl("some-url");
    bracketClient.getTournamentByUrl("another-url");

    verify(restTemplate, times(2)).getForObject(any(), any(), anyMap());
    verifyNoMoreInteractions(restTemplate);
  }

  @Test
  public void noCacheTournamentIdNotFound() {
    Tournament tournament = new Tournament();

    when(restTemplate.getForObject(any(), any(), anyMap())).thenThrow(new HttpClientErrorException(HttpStatus.NOT_FOUND));

    bracketClient.getTournamentByUrl("some-url-2");
    bracketClient.getTournamentByUrl("another-url-2");

    verify(restTemplate, times(2)).getForObject(any(), any(), anyMap());
    verifyNoMoreInteractions(restTemplate);
  }

  @Test
  public void cachesMatchByPlayers() {
    Match match = mock(Match.class);
    when(match.getTeam1()).thenReturn(determinedTeamMock);
    when(match.getTeam2()).thenReturn(determinedTeamMock);

    MatchByPlayersRequest request = new MatchByPlayersRequest(1, false, 2, 3, 4L, 5L);

    when(restTemplate.postForObject(anyString(), eq(request), eq(Match.class))).thenReturn(match);

    bracketClient.getMatchByPlayers(request);
    bracketClient.getMatchByPlayers(request);

    verify(restTemplate, times(1)).postForObject(anyString(), eq(request), eq(Match.class));
    verifyNoMoreInteractions(restTemplate);
  }

  @Test
  public void notCachesMatchByPlayersWhenTeam1Undetermined() {
    Match match = mock(Match.class);
    when(match.getTeam1()).thenReturn(undeterminedTeamMock);
    when(match.getTeam2()).thenReturn(determinedTeamMock);

    MatchByPlayersRequest request = new MatchByPlayersRequest(2, false, 2, 3, 4L, 5L);

    when(restTemplate.postForObject(anyString(), eq(request), eq(Match.class))).thenReturn(match);

    bracketClient.getMatchByPlayers(request);
    bracketClient.getMatchByPlayers(request);

    verify(restTemplate, times(2)).postForObject(anyString(), eq(request), eq(Match.class));
    verifyNoMoreInteractions(restTemplate);
  }

  @Test
  public void notCachesMatchByPlayersWhenTeam2Undetermined() {
    Match match = mock(Match.class);
    when(match.getTeam1()).thenReturn(determinedTeamMock);
    when(match.getTeam2()).thenReturn(undeterminedTeamMock);

    MatchByPlayersRequest request = new MatchByPlayersRequest(3, false, 2, 3, 4L, 5L);

    when(restTemplate.postForObject(anyString(), eq(request), eq(Match.class))).thenReturn(match);

    bracketClient.getMatchByPlayers(request);
    bracketClient.getMatchByPlayers(request);

    verify(restTemplate, times(2)).postForObject(anyString(), eq(request), eq(Match.class));
    verifyNoMoreInteractions(restTemplate);
  }

  @Test
  public void notCachesMatchByPlayersWhenResultEmpty() {
    MatchByPlayersRequest request = new MatchByPlayersRequest(4, false, 2, 3, 4L, 5L);

    when(restTemplate.postForObject(anyString(), same(request), eq(Match.class))).thenReturn(null);

    bracketClient.getMatchByPlayers(request);
    bracketClient.getMatchByPlayers(request);

    verify(restTemplate, times(2)).postForObject(anyString(), eq(request), eq(Match.class));
    verifyNoMoreInteractions(restTemplate);
  }

  @Test
  public void matchCachedMethodsSameConditionDifferentCacheName() {
    // Get all methods returning Optional<Match>
    List<Method> matchMethods = getMatchMethods();

    // Get the annotation on the first method
    Method firstMethod = matchMethods.get(0);
    Cacheable cacheableAnnotation = firstMethod.getAnnotation(Cacheable.class);

    // Verify other methods have the same "unless" and "condition"
    for (int i = 1; i < matchMethods.size(); i++) {
      Method otherMethod = matchMethods.get(i);
      Cacheable otherCacheableAnnotation = otherMethod.getAnnotation(Cacheable.class);

      assertEquals(cacheableAnnotation.condition(), otherCacheableAnnotation.condition());
    }

    // Verify cache names are different
    long count = matchMethods.stream()
        .map(m -> m.getAnnotation(Cacheable.class))
        .map(Cacheable::value)
        .distinct()
        .count();
    assertEquals(matchMethods.size(), count);
  }

  private List<Method> getMatchMethods() {
    Method[] methods = BracketClientImpl.class.getMethods();
    List<Method> matchMethods = Arrays.stream(methods)
        .filter(method -> method.getGenericReturnType() instanceof ParameterizedType)
        .filter(method -> ((ParameterizedType) method.getGenericReturnType()).getActualTypeArguments().length == 1)
        .filter(method -> ((ParameterizedType) method.getGenericReturnType()).getActualTypeArguments()[0].getTypeName().equals(Match.class.getName()))
        .collect(Collectors.toList());
    assertEquals(4, matchMethods.size());
    return matchMethods;
  }
}
