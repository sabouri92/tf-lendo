package com.mrsabouri.tennisfollow.bracketclient;

import com.mrsabouri.tennisfollow.bracketclient.impl.BracketClientImpl;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
@EnableCaching
public class TestConfiguration {
  @Bean
  public CacheManager cacheManager() {
    return new ConcurrentMapCacheManager();
  }

  @Bean
  public BracketClient bracketClient(RestTemplate restTemplate) {
    return new BracketClientImpl(restTemplate);
  }
}
