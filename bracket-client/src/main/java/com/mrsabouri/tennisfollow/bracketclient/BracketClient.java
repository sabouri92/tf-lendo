package com.mrsabouri.tennisfollow.bracketclient;

import com.mrsabouri.tennisfollow.bracketclient.model.Match;
import com.mrsabouri.tennisfollow.bracketclient.model.MatchByPlayersRequest;
import com.mrsabouri.tennisfollow.bracketclient.model.Tournament;
import com.mrsabouri.tennisfollow.bracketclient.model.enums.BracketType;
import com.mrsabouri.tennisfollow.bracketclient.model.enums.Round;

import java.util.Optional;

public interface BracketClient {
  Optional<Tournament> getTournamentByUrl(String tournamentUrl);

  Optional<Tournament> getTournamentById(long tournamentId);

  Optional<Match> getMatchByPlayers(MatchByPlayersRequest matchByPlayersRequest);

  Optional<Match> getNextRoundMatchByPlayers(MatchByPlayersRequest matchByPlayersRequest);

  Optional<Match> getMatchByRoundIndex(long tournamentId, BracketType bracketType, Round round, int roundIndex);

  Optional<Match> getMatchById(long matchId);

  Round getPreviousRound(Round round);
}
