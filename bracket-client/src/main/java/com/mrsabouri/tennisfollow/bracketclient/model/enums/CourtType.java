package com.mrsabouri.tennisfollow.bracketclient.model.enums;

public enum CourtType {
  CLAY,
  HARD,
  GRASS
}
