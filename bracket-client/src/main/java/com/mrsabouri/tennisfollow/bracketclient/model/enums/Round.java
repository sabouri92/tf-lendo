package com.mrsabouri.tennisfollow.bracketclient.model.enums;

public enum Round {
  F,
  SF,
  QF,
  R16,
  R32,
  R64,
  R128,
  Q1,
  Q2,
  Q3,
  RR
}
