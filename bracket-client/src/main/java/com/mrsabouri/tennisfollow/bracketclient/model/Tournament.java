package com.mrsabouri.tennisfollow.bracketclient.model;

import com.mrsabouri.tennisfollow.bracketclient.model.enums.CourtType;
import com.mrsabouri.tennisfollow.bracketclient.model.enums.TourCategory;

import java.time.LocalDate;

public class Tournament {
  private long id;
  private int season;
  private String title;
  private TourCategory category;
  private boolean indoors;
  private CourtType courtType;
  private String location;
  private LocalDate startDate;
  private LocalDate endDate;

  public long getId() {
    return id;
  }

  public int getSeason() {
    return season;
  }

  public String getTitle() {
    return title;
  }

  public TourCategory getCategory() {
    return category;
  }

  public boolean isIndoors() {
    return indoors;
  }

  public CourtType getCourtType() {
    return courtType;
  }

  public String getLocation() {
    return location;
  }

  public LocalDate getStartDate() {
    return startDate;
  }

  public LocalDate getEndDate() {
    return endDate;
  }
}
