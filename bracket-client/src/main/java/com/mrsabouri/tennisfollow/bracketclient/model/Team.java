package com.mrsabouri.tennisfollow.bracketclient.model;

public class Team {
  private boolean bye;
  private boolean playersDetermined;
  private Integer seedNumber;
  private Long player1Id;
  private Long player2Id;

  public boolean isBye() {
    return bye;
  }

  public boolean isPlayersDetermined() {
    return playersDetermined;
  }

  public Integer getSeedNumber() {
    return seedNumber;
  }

  public Long getPlayer1Id() {
    return player1Id;
  }

  public Long getPlayer2Id() {
    return player2Id;
  }
}
