package com.mrsabouri.tennisfollow.bracketclient.model.enums;

public enum BracketType {
  SINGLES,
  DOUBLES,
  QUALIFIERS
}
