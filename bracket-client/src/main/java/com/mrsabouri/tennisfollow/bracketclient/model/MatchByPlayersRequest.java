package com.mrsabouri.tennisfollow.bracketclient.model;

import java.util.Objects;

public class MatchByPlayersRequest {
  private long tournamentId;
  private boolean qualifiers;
  private long team1Player1;
  private long team2Player1;
  private Long team1Player2;
  private Long team2Player2;

  public MatchByPlayersRequest(long tournamentId, boolean qualifiers, long team1Player1, long team2Player1, Long team1Player2, Long team2Player2) {
    this.tournamentId = tournamentId;
    this.qualifiers = qualifiers;
    this.team1Player1 = team1Player1;
    this.team2Player1 = team2Player1;
    this.team1Player2 = team1Player2;
    this.team2Player2 = team2Player2;
  }

  public long getTournamentId() {
    return tournamentId;
  }

  public boolean isQualifiers() {
    return qualifiers;
  }

  public long getTeam1Player1() {
    return team1Player1;
  }

  public long getTeam2Player1() {
    return team2Player1;
  }

  public Long getTeam1Player2() {
    return team1Player2;
  }

  public Long getTeam2Player2() {
    return team2Player2;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    MatchByPlayersRequest request = (MatchByPlayersRequest) o;
    return tournamentId == request.tournamentId &&
        qualifiers == request.qualifiers &&
        team1Player1 == request.team1Player1 &&
        team2Player1 == request.team2Player1 &&
        Objects.equals(team1Player2, request.team1Player2) &&
        Objects.equals(team2Player2, request.team2Player2);
  }

  @Override
  public int hashCode() {
    return Objects.hash(tournamentId, qualifiers, team1Player1, team2Player1, team1Player2, team2Player2);
  }
}
