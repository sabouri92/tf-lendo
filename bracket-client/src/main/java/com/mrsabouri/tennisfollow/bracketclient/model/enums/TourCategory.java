package com.mrsabouri.tennisfollow.bracketclient.model.enums;

public enum TourCategory {
  ATP_250,
  ATP_500,
  ATP_1000,
  ATP_FINALS,
  GRAND_SLAM
}
