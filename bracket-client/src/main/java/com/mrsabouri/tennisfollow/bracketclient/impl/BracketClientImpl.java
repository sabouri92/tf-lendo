package com.mrsabouri.tennisfollow.bracketclient.impl;

import com.mrsabouri.tennisfollow.bracketclient.BracketClient;
import com.mrsabouri.tennisfollow.bracketclient.model.Match;
import com.mrsabouri.tennisfollow.bracketclient.model.MatchByPlayersRequest;
import com.mrsabouri.tennisfollow.bracketclient.model.Tournament;
import com.mrsabouri.tennisfollow.bracketclient.model.enums.BracketType;
import com.mrsabouri.tennisfollow.bracketclient.model.enums.Round;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;

public class BracketClientImpl implements BracketClient {
  private static final String BASE_URL = "http://bracket/";
  private static final String MATCH_CACHE_CONDITION = "#result == null" +
      "|| !#result.getTeam1().isPlayersDetermined() " +
      "|| !#result.getTeam2().isPlayersDetermined()";

  private RestTemplate restTemplate;
  private String baseUrl;

  public BracketClientImpl(RestTemplate restTemplate) {
    this(restTemplate, BASE_URL);
  }

  public BracketClientImpl(RestTemplate restTemplate, String baseUrl) {
    this.restTemplate = restTemplate;
    this.baseUrl = baseUrl;
  }

  @Override
  @Cacheable(value = "bracket/tournament/by-url")
  public Optional<Tournament> getTournamentByUrl(String tournamentUrl) {
    String apiUrl = baseUrl + "/tournament/by-url?url={url}";

    try {
      return Optional.ofNullable(restTemplate.getForObject(apiUrl, Tournament.class, Map.of("url", tournamentUrl)));
    } catch (HttpClientErrorException hceex) {
      if (hceex.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
        return Optional.empty();
      }
      throw hceex;
    }
  }

  @Override
  @Cacheable(value = "bracket/tournament/by-id")
  public Optional<Tournament> getTournamentById(long tournamentId) {
    String apiUrl = baseUrl + "/tournament/by-id/{id}";

    try {
      return Optional.ofNullable(restTemplate.getForObject(apiUrl, Tournament.class, Map.of("id", tournamentId)));
    } catch (HttpClientErrorException hceex) {
      if (hceex.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
        return Optional.empty();
      }
      throw hceex;
    }
  }

  @Override
  @Cacheable(value = "bracket/match/by-players", unless = MATCH_CACHE_CONDITION)
  public Optional<Match> getMatchByPlayers(MatchByPlayersRequest matchByPlayersRequest) {
    String apiUrl = baseUrl + "/match/by-players";
    return callByPlayers(apiUrl, matchByPlayersRequest);
  }

  @Override
  @Cacheable(value = "bracket/match/next-round", unless = MATCH_CACHE_CONDITION)
  public Optional<Match> getNextRoundMatchByPlayers(MatchByPlayersRequest matchByPlayersRequest) {
    String apiUrl = baseUrl + "/match/next-round";
    return callByPlayers(apiUrl, matchByPlayersRequest);
  }

  @Override
  @Cacheable(value = "bracket/match/by-round-index", unless = MATCH_CACHE_CONDITION)
  public Optional<Match> getMatchByRoundIndex(long tournamentId, BracketType bracketType, Round round, int roundIndex) {
    String apiUrl = baseUrl + "/match/by-round-index?tournamentId={tournamentId}&bracketType={bracketType}&round={round}&roundIndex={roundIndex}";
    return callByParams(apiUrl, Map.of("tournamentId", tournamentId, "bracketType", bracketType, "round", round, "roundIndex", roundIndex));
  }

  @Override
  @Cacheable(value = "bracket/match/by-id", unless = MATCH_CACHE_CONDITION)
  public Optional<Match> getMatchById(long matchId) {
    String apiUrl = baseUrl + "/match/by-id/{matchId}";
    return callByMatchId(apiUrl, matchId);
  }

  @Override
  public Round getPreviousRound(Round round) {
    switch (round) {
      case F:
        return Round.SF;

      case SF:
        return Round.QF;

      case QF:
        return Round.R16;

      case R16:
        return Round.R32;

      case R32:
        return Round.R64;

      case R64:
        return Round.R128;

      case R128:
        throw new IllegalArgumentException();

      case Q1:
        throw new IllegalArgumentException();

      case Q2:
        return Round.Q1;

      case Q3:
        return Round.Q2;

      case RR:
        throw new IllegalArgumentException();

      default:
        throw new IllegalArgumentException();
    }
  }

  private Optional<Match> callByPlayers(String apiUrl, MatchByPlayersRequest matchByPlayersRequest) {
    return getBySupplier(() -> restTemplate.postForObject(apiUrl, matchByPlayersRequest, Match.class));
  }

  private Optional<Match> callByMatchId(String apiUrl, long matchId) {
    return getBySupplier(() -> restTemplate.getForObject(apiUrl, Match.class, Map.of("matchId", matchId)));
  }

  private Optional<Match> callByParams(String apiUrl, Map<String, ?> params) {
    return getBySupplier(() -> restTemplate.getForObject(apiUrl, Match.class, params));
  }

  private Optional<Match> getBySupplier(Supplier<Match> supplier) {

    try {
      Match match = supplier.get();
      return Optional.ofNullable(match);
    } catch (HttpClientErrorException hceex) {
      if (hceex.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
        return Optional.empty();
      }
      throw hceex;
    }
  }
}
