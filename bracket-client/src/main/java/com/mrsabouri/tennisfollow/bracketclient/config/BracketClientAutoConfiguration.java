package com.mrsabouri.tennisfollow.bracketclient.config;

import com.mrsabouri.tennisfollow.bracketclient.BracketClient;
import com.mrsabouri.tennisfollow.bracketclient.impl.BracketClientImpl;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
@ConditionalOnBean(RestTemplate.class)
public class BracketClientAutoConfiguration {
  @Bean
  public BracketClient bracketClient(RestTemplate restTemplate) {
    return new BracketClientImpl(restTemplate);
  }
}
