package com.mrsabouri.tennisfollow.bracketclient.model;

import com.mrsabouri.tennisfollow.bracketclient.model.enums.BracketType;
import com.mrsabouri.tennisfollow.bracketclient.model.enums.Round;

public class Match {
  private long id;
  private long tournamentId;
  private BracketType bracketType;
  private Round round;
  private int roundIndex;
  private Team team1;
  private Team team2;

  public long getId() {
    return id;
  }

  public long getTournamentId() {
    return tournamentId;
  }

  public BracketType getBracketType() {
    return bracketType;
  }

  public Round getRound() {
    return round;
  }

  public int getRoundIndex() {
    return roundIndex;
  }

  public Team getTeam1() {
    return team1;
  }

  public Team getTeam2() {
    return team2;
  }
}
