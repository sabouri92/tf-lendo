package com.mrsabouri.tennisfollow.orderofplay.enums;

public enum MatchType {
  SINGLES,
  DOUBLES
}
