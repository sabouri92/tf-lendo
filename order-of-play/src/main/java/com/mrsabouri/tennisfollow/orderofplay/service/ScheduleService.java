package com.mrsabouri.tennisfollow.orderofplay.service;

import com.mrsabouri.tennisfollow.orderofplay.service.model.ScheduleDay;

import java.time.LocalDate;
import java.util.List;

public interface ScheduleService {
  List<ScheduleDay> getScheduleDays();
  ScheduleDay scheduleForDay(LocalDate day);
}
