package com.mrsabouri.tennisfollow.orderofplay.service.persistence.entity;

import com.mrsabouri.tennisfollow.orderofplay.scraper.persistence.entity.UnknownMatchEntity;

import javax.persistence.*;
import java.time.Instant;

@Table(
    name = "match",
    uniqueConstraints = @UniqueConstraint(columnNames = {"court_schedule_id", "index"})
)
@Entity
public class MatchEntity {
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE)
  private Long id;

  @ManyToOne(optional = false)
  @JoinColumn(updatable = false)
  private CourtScheduleEntity courtSchedule;

  @Column(nullable = false, updatable = false)
  private int index;

  @Column
  private Instant notBefore;

  @Column
  private Long matchId;

  @OneToOne(mappedBy = "match", cascade = CascadeType.ALL)
  private UnknownMatchEntity unknownMatch;

  public MatchEntity() {
  }

  public MatchEntity(CourtScheduleEntity courtSchedule, int index) {
    this.courtSchedule = courtSchedule;
    this.index = index;
  }

  public static MatchEntity of(CourtScheduleEntity courtSchedule, int index) {
    return new MatchEntity(courtSchedule, index);
  }

  public CourtScheduleEntity getCourtSchedule() {
    return courtSchedule;
  }

  public int getIndex() {
    return index;
  }

  public Instant getNotBefore() {
    return notBefore;
  }

  public void setNotBefore(Instant notBefore) {
    this.notBefore = notBefore;
  }

  public Long getMatchId() {
    return matchId;
  }

  public void setMatchId(Long matchId) {
    this.matchId = matchId;
  }

  public UnknownMatchEntity getUnknownMatch() {
    return unknownMatch;
  }

  public void setUnknownMatch(UnknownMatchEntity unknownMatch) {
    this.unknownMatch = unknownMatch;
  }
}
