package com.mrsabouri.tennisfollow.orderofplay.service.model;

import java.util.List;

public class CourtSchedule {
  private long tournamentId;
  private String courtName;
  private List<Match> matches;

  public CourtSchedule(long tournamentId, String courtName, List<Match> matches) {
    this.tournamentId = tournamentId;
    this.courtName = courtName;
    this.matches = matches;
  }

  public long getTournamentId() {
    return tournamentId;
  }

  public String getCourtName() {
    return courtName;
  }

  public List<Match> getMatches() {
    return matches;
  }
}
