package com.mrsabouri.tennisfollow.orderofplay.scraper.service.impl;

import com.mrsabouri.tennisfollow.bracketclient.BracketClient;
import com.mrsabouri.tennisfollow.bracketclient.model.Tournament;
import com.mrsabouri.tennisfollow.orderofplay.scraper.model.ScrapedCourtSchedule;
import com.mrsabouri.tennisfollow.orderofplay.scraper.model.ScrapedTournamentCourts;
import com.mrsabouri.tennisfollow.orderofplay.scraper.service.TournamentCourtsCleaner;
import com.mrsabouri.tennisfollow.orderofplay.service.persistence.entity.CourtScheduleEntity;
import com.mrsabouri.tennisfollow.orderofplay.service.persistence.repository.CourtScheduleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Component
public class TournamentCourtsCleanerImpl implements TournamentCourtsCleaner {
  private BracketClient bracketClient;
  private CourtScheduleRepository courtScheduleRepository;

  @Autowired
  public TournamentCourtsCleanerImpl(BracketClient bracketClient, CourtScheduleRepository courtScheduleRepository) {
    this.bracketClient = bracketClient;
    this.courtScheduleRepository = courtScheduleRepository;
  }

  @Override
  public void cleanInvalidCourts(ScrapedTournamentCourts tournamentCourts) {
    LocalDate day = tournamentCourts.getDay();
    Optional<Tournament> tournamentOptional = bracketClient.getTournamentByUrl(tournamentCourts.getTournamentUrl());

    if (tournamentOptional.isPresent()) {
      long tournamentId = tournamentOptional.get().getId();
      List<ScrapedCourtSchedule> scrapedCourtSchedules = tournamentCourts.getCourtSchedules();
      List<CourtScheduleEntity> courtScheduleEntities = courtScheduleRepository.findByTournamentIdAndDay(tournamentId, day);

      for (CourtScheduleEntity courtScheduleEntity : courtScheduleEntities) {
        boolean present = scrapedCourtSchedules.stream()
            .anyMatch(scrapedCourtSchedule -> scrapedCourtSchedule.getCourtName().equals(courtScheduleEntity.getCourtName()));
        if (!present) {
          courtScheduleRepository.delete(courtScheduleEntity);
        }
      }
    }
  }
}
