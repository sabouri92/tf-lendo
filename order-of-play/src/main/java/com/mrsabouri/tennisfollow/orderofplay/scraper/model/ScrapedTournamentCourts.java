package com.mrsabouri.tennisfollow.orderofplay.scraper.model;

import java.time.LocalDate;
import java.util.List;

public class ScrapedTournamentCourts {
  private String tournamentUrl;
  private String tournamentLocation;
  private LocalDate day;
  private List<ScrapedCourtSchedule> courtSchedules;

  public ScrapedTournamentCourts(String tournamentUrl, String tournamentLocation, LocalDate day, List<ScrapedCourtSchedule> courtSchedules) {
    this.tournamentUrl = tournamentUrl;
    this.tournamentLocation = tournamentLocation;
    this.day = day;
    this.courtSchedules = courtSchedules;
  }

  public String getTournamentUrl() {
    return tournamentUrl;
  }

  public String getTournamentLocation() {
    return tournamentLocation;
  }

  public LocalDate getDay() {
    return day;
  }

  public List<ScrapedCourtSchedule> getCourtSchedules() {
    return courtSchedules;
  }

  @Override
  public String toString() {
    return "ScrapedTournamentCourts{" +
        "tournamentUrl='" + tournamentUrl + '\'' +
        ", tournamentLocation='" + tournamentLocation + '\'' +
        ", day=" + day +
        ", courtSchedules=" + courtSchedules +
        '}';
  }
}
