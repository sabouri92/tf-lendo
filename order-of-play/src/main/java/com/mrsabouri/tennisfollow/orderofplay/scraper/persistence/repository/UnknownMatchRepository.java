package com.mrsabouri.tennisfollow.orderofplay.scraper.persistence.repository;

import com.mrsabouri.tennisfollow.orderofplay.scraper.persistence.entity.UnknownMatchEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UnknownMatchRepository extends JpaRepository<UnknownMatchEntity, Long> {
}
