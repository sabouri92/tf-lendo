package com.mrsabouri.tennisfollow.orderofplay.service.impl;

import com.mrsabouri.tennisfollow.orderofplay.enums.MatchType;
import com.mrsabouri.tennisfollow.orderofplay.scraper.persistence.entity.UnknownMatchEntity;
import com.mrsabouri.tennisfollow.orderofplay.scraper.persistence.entity.UnknownTeam;
import com.mrsabouri.tennisfollow.orderofplay.service.ScheduleService;
import com.mrsabouri.tennisfollow.orderofplay.service.model.CourtSchedule;
import com.mrsabouri.tennisfollow.orderofplay.service.model.Match;
import com.mrsabouri.tennisfollow.orderofplay.service.model.ScheduleDay;
import com.mrsabouri.tennisfollow.orderofplay.service.model.Team;
import com.mrsabouri.tennisfollow.orderofplay.service.persistence.entity.MatchEntity;
import com.mrsabouri.tennisfollow.orderofplay.service.persistence.repository.CourtScheduleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class ScheduleServiceImpl implements ScheduleService {
  private CourtScheduleRepository courtScheduleRepository;

  @Autowired
  public ScheduleServiceImpl(CourtScheduleRepository courtScheduleRepository) {
    this.courtScheduleRepository = courtScheduleRepository;
  }

  @Override
  public List<ScheduleDay> getScheduleDays() {
    return courtScheduleRepository.allDays()
        .stream()
        .map(ScheduleDay::new)
        .collect(Collectors.toList());
  }

  @Override
  public ScheduleDay scheduleForDay(LocalDate day) {

    List<CourtSchedule> courtSchedules = courtScheduleRepository.findByDay(day)
        .stream()
        .map(
            courtScheduleEntity -> new CourtSchedule(
                courtScheduleEntity.getTournamentId(),
                courtScheduleEntity.getCourtName(),
                courtScheduleEntity.getMatches().stream()
                    .map(this::toModelMatch)
                    .collect(Collectors.toList())
            )
        )
        .collect(Collectors.toList());

    return new ScheduleDay(
        day,
        courtSchedules
    );
  }

  private Match toModelMatch(MatchEntity matchEntity) {
    if (matchEntity.getMatchId() != null) {
      return new Match(
          matchEntity.getMatchId(),
          matchEntity.getNotBefore()
      );

    } else {
      UnknownMatchEntity unknownMatchEntity = matchEntity.getUnknownMatch();
      MatchType matchType = matchEntity.getUnknownMatch().getMatchType();

      return new Match(
          toModelTeam(unknownMatchEntity.getTeam1(), matchType),
          toModelTeam(unknownMatchEntity.getTeam2(), matchType),
          toModelTeam(unknownMatchEntity.getAlternativeTeam1(), matchType),
          toModelTeam(unknownMatchEntity.getAlternativeTeam2(), matchType),
          matchType,
          matchEntity.getNotBefore()
      );
    }
  }

  private Team toModelTeam(UnknownTeam unknownTeam, MatchType matchType) {
    if (unknownTeam == null) {
      return null;
    }

    if (matchType == MatchType.SINGLES) {
      return Team.singles(unknownTeam.getName1());
    } else {
      return Team.doubles(unknownTeam.getName1(), unknownTeam.getName2());
    }
  }
}
