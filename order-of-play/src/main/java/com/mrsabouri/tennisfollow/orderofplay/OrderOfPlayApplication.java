package com.mrsabouri.tennisfollow.orderofplay;

import ch.qos.logback.access.tomcat.LogbackValve;
import com.mrsabouri.lib.jsouputils.DocumentLoader;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.client.RestTemplate;

@EnableAsync
@EnableCaching
@EnableJpaRepositories
@SpringBootApplication
public class OrderOfPlayApplication {

  @Bean
  public WebServerFactoryCustomizer<TomcatServletWebServerFactory> webServerFactoryCustomizer() {
    return factory -> factory.addContextValves(new LogbackValve());
  }

  @Bean
  public RestTemplate restTemplate() {
    return new RestTemplate();
  }

  @Bean
  public DocumentLoader documentLoader(RestTemplate restTemplate) {
    return new DocumentLoader(restTemplate);
  }

  public static void main(String[] args) {
    SpringApplication.run(OrderOfPlayApplication.class, args);
  }
}
