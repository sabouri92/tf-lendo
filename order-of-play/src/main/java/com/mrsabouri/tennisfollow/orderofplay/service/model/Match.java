package com.mrsabouri.tennisfollow.orderofplay.service.model;

import com.mrsabouri.tennisfollow.orderofplay.enums.MatchType;

import java.time.Instant;

public class Match {
  private RecognizedBy recognizedBy;
  private Long matchId;
  private Team team1;
  private Team team2;
  private Team alternativeTeam1;
  private Team alternativeTeam2;
  private MatchType matchType;
  private Instant notBefore;

  public Match() {
  }

  public Match(Long matchId, Instant notBefore) {
    this.notBefore = notBefore;
    this.recognizedBy = RecognizedBy.MATCH_ID;
    this.matchId = matchId;
  }

  public Match(Team team1, Team team2, Team alternativeTeam1, Team alternativeTeam2, MatchType matchType, Instant notBefore) {
    this.recognizedBy = RecognizedBy.PLAYER_NAMES;
    this.team1 = team1;
    this.team2 = team2;
    this.alternativeTeam1 = alternativeTeam1;
    this.alternativeTeam2 = alternativeTeam2;
    this.matchType = matchType;
    this.notBefore = notBefore;
  }

  public RecognizedBy getRecognizedBy() {
    return recognizedBy;
  }

  public Long getMatchId() {
    return matchId;
  }

  public Team getTeam1() {
    return team1;
  }

  public Team getTeam2() {
    return team2;
  }

  public Team getAlternativeTeam1() {
    return alternativeTeam1;
  }

  public Team getAlternativeTeam2() {
    return alternativeTeam2;
  }

  public MatchType getMatchType() {
    return matchType;
  }

  public Instant getNotBefore() {
    return notBefore;
  }
}
