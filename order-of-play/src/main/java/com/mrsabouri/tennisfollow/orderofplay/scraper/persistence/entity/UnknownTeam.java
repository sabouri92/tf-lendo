package com.mrsabouri.tennisfollow.orderofplay.scraper.persistence.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class UnknownTeam {
  @Column
  private String url1;

  @Column
  private String name1;

  @Column
  private String url2;

  @Column
  private String name2;

  public UnknownTeam() {
  }

  public UnknownTeam(String url1, String name1, String url2, String name2) {
    this.url1 = url1;
    this.name1 = name1;
    this.url2 = url2;
    this.name2 = name2;
  }

  public String getUrl1() {
    return url1;
  }

  public String getName1() {
    return name1;
  }

  public String getUrl2() {
    return url2;
  }

  public String getName2() {
    return name2;
  }
}
