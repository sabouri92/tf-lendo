package com.mrsabouri.tennisfollow.orderofplay.scraper.model;

import com.mrsabouri.tennisfollow.orderofplay.enums.MatchType;
import com.mrsabouri.tennisfollow.orderofplay.enums.Stage;

import java.time.LocalTime;

public class ScrapedMatch {
  private LocalTime notBefore;
  private MatchType matchType;
  private Stage stage;
  private ScrapedTeam team1;
  private ScrapedTeam team2;
  private ScrapedTeam alternativeTeam1;
  private ScrapedTeam alternativeTeam2;

  public ScrapedMatch(LocalTime notBefore, MatchType matchType, Stage stage, ScrapedTeam team1, ScrapedTeam team2) {
    this.notBefore = notBefore;
    this.matchType = matchType;
    this.stage = stage;
    this.team1 = team1;
    this.team2 = team2;
    this.alternativeTeam1 = null;
    this.alternativeTeam2 = null;
  }

  public ScrapedMatch(LocalTime notBefore, MatchType matchType, Stage stage, ScrapedTeam team1, ScrapedTeam team2, ScrapedTeam alternativeTeam1, ScrapedTeam alternativeTeam2) {
    this.notBefore = notBefore;
    this.matchType = matchType;
    this.stage = stage;
    this.team1 = team1;
    this.team2 = team2;
    this.alternativeTeam1 = alternativeTeam1;
    this.alternativeTeam2 = alternativeTeam2;
  }

  public LocalTime getNotBefore() {
    return notBefore;
  }

  public MatchType getMatchType() {
    return matchType;
  }

  public Stage getStage() {
    return stage;
  }

  public ScrapedTeam getTeam1() {
    return team1;
  }

  public ScrapedTeam getTeam2() {
    return team2;
  }

  public ScrapedTeam getAlternativeTeam1() {
    return alternativeTeam1;
  }

  public ScrapedTeam getAlternativeTeam2() {
    return alternativeTeam2;
  }

  @Override
  public String toString() {
    return "ScrapedMatch{" +
        "notBefore=" + notBefore +
        ", matchType=" + matchType +
        ", stage=" + stage +
        ", team1=" + team1 +
        ", team2=" + team2 +
        ", alternativeTeam1=" + alternativeTeam1 +
        ", alternativeTeam2=" + alternativeTeam2 +
        '}';
  }
}
