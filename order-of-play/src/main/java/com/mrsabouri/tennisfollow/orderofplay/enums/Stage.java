package com.mrsabouri.tennisfollow.orderofplay.enums;

public enum Stage {
  QUALIFIER,
  MAIN_DRAW,
  ROUND_ROBIN
}
