package com.mrsabouri.tennisfollow.orderofplay.scraper.service.impl;

import com.mrsabouri.tennisfollow.bracketclient.BracketClient;
import com.mrsabouri.tennisfollow.bracketclient.model.Match;
import com.mrsabouri.tennisfollow.bracketclient.model.MatchByPlayersRequest;
import com.mrsabouri.tennisfollow.bracketclient.model.Tournament;
import com.mrsabouri.tennisfollow.orderofplay.enums.MatchType;
import com.mrsabouri.tennisfollow.orderofplay.enums.Stage;
import com.mrsabouri.tennisfollow.orderofplay.scraper.model.ScrapedCourtSchedule;
import com.mrsabouri.tennisfollow.orderofplay.scraper.model.ScrapedMatch;
import com.mrsabouri.tennisfollow.orderofplay.scraper.model.ScrapedTeam;
import com.mrsabouri.tennisfollow.orderofplay.scraper.model.ScrapedTournamentCourts;
import com.mrsabouri.tennisfollow.orderofplay.scraper.persistence.entity.UnknownMatchEntity;
import com.mrsabouri.tennisfollow.orderofplay.scraper.persistence.entity.UnknownTeam;
import com.mrsabouri.tennisfollow.orderofplay.scraper.service.ScrapedScheduleToEntityConverter;
import com.mrsabouri.tennisfollow.orderofplay.scraper.service.ZoneIdService;
import com.mrsabouri.tennisfollow.orderofplay.service.persistence.entity.CourtScheduleEntity;
import com.mrsabouri.tennisfollow.orderofplay.service.persistence.entity.MatchEntity;
import com.mrsabouri.tennisfollow.orderofplay.service.persistence.repository.CourtScheduleRepository;
import com.mrsabouri.tennisfollow.playerregistry.PlayerRegistryClient;
import com.mrsabouri.tennisfollow.playerregistry.model.PlayerByUrlResponse;
import com.mrsabouri.tennisfollow.playerregistry.model.PlayerByUrlStatus;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class ScrapedScheduleToEntityConverterImpl implements ScrapedScheduleToEntityConverter {
  private final Logger LOGGER = LoggerFactory.getLogger(getClass());

  private BracketClient bracketClient;
  private CourtScheduleRepository courtScheduleRepository;
  private PlayerRegistryClient playerRegistryClient;
  private ZoneIdService zoneIdService;

  @Autowired
  public ScrapedScheduleToEntityConverterImpl(BracketClient bracketClient,
                                              CourtScheduleRepository courtScheduleRepository,
                                              PlayerRegistryClient playerRegistryClient,
                                              ZoneIdService zoneIdService) {
    this.bracketClient = bracketClient;
    this.courtScheduleRepository = courtScheduleRepository;
    this.playerRegistryClient = playerRegistryClient;
    this.zoneIdService = zoneIdService;
  }

  @Override
  public CourtScheduleEntity convert(ScrapedTournamentCourts tournamentCourts, ScrapedCourtSchedule scrapedCourtSchedule) {
    String tournamentUrl = tournamentCourts.getTournamentUrl();
    LocalDate day = tournamentCourts.getDay();
    ZoneId zoneId = zoneIdService.fromLocation(tournamentCourts.getTournamentLocation(), day);

    long tournamentId = getTournamentId(tournamentUrl);

    CourtScheduleEntity courtScheduleEntity = courtScheduleRepository
        .findByTournamentIdAndDayAndCourtName(tournamentId, day, scrapedCourtSchedule.getCourtName())
        .orElse(CourtScheduleEntity.of(tournamentId, day, scrapedCourtSchedule.getCourtName()));

    // Drop excessive existing matches
    if (courtScheduleEntity.getMatches() != null) {
      courtScheduleEntity.setMatches(
          courtScheduleEntity.getMatches()
              .stream()
              .limit(scrapedCourtSchedule.getScrapedMatches().size())
              .collect(Collectors.toList())
      );
    }

    for (int i = 0; i < scrapedCourtSchedule.getScrapedMatches().size(); i++) {
      // Get scraped match and target match entity
      ScrapedMatch scrapedMatch = scrapedCourtSchedule.getScrapedMatches().get(i);
      MatchEntity matchEntity = getMatchEntityAtIndex(courtScheduleEntity, i);

      // Set not before
      if (scrapedMatch.getNotBefore() != null) {
        matchEntity.setNotBefore(day.atTime(scrapedMatch.getNotBefore()).atZone(zoneId).toInstant());
      } else {
        matchEntity.setNotBefore(null);
      }

      // Set matchId or unknown match
      Optional<Long> matchId = getMatchId(scrapedMatch, tournamentId);
      if (matchId.isEmpty()) {
        UnknownMatchEntity unknownMatchEntity = unknownMatch(matchEntity, scrapedMatch);
        matchEntity.setMatchId(null);
        matchEntity.setUnknownMatch(unknownMatchEntity);
      } else {
        matchEntity.setMatchId(matchId.get());
      }
    }

    return courtScheduleEntity;
  }

  private MatchEntity getMatchEntityAtIndex(CourtScheduleEntity entity, int idx) {
    if (entity.getMatches() != null && idx < entity.getMatches().size()) {
      MatchEntity matchEntity = entity.getMatches().get(idx);
      Validate.isTrue(matchEntity.getIndex() == idx);
      return matchEntity;

    } else {
      MatchEntity matchEntity = MatchEntity.of(entity, idx);
      entity.addMatch(matchEntity);
      return matchEntity;
    }
  }

  private UnknownMatchEntity unknownMatch(MatchEntity matchEntity, ScrapedMatch scrapedMatch) {
    UnknownTeam team1 = unknownTeam(scrapedMatch.getTeam1());
    UnknownTeam team2 = unknownTeam(scrapedMatch.getTeam2());
    UnknownTeam alternativeTeam1 = unknownTeam(scrapedMatch.getAlternativeTeam1());
    UnknownTeam alternativeTeam2 = unknownTeam(scrapedMatch.getAlternativeTeam2());

    if (matchEntity.getUnknownMatch() != null) {
      UnknownMatchEntity unknownMatchEntity = matchEntity.getUnknownMatch();
      unknownMatchEntity.setMatchType(scrapedMatch.getMatchType());
      unknownMatchEntity.setTeam1(team1);
      unknownMatchEntity.setTeam2(team2);
      unknownMatchEntity.setAlternativeTeam1(alternativeTeam1);
      unknownMatchEntity.setAlternativeTeam2(alternativeTeam2);
      return unknownMatchEntity;

    } else {
      return new UnknownMatchEntity(
          matchEntity,
          scrapedMatch.getMatchType(),
          team1,
          team2,
          alternativeTeam1,
          alternativeTeam2
      );
    }
  }

  private UnknownTeam unknownTeam(ScrapedTeam scrapedTeam) {
    if (scrapedTeam == null) {
      return new UnknownTeam();
    }

    return new UnknownTeam(scrapedTeam.getUrl1(), scrapedTeam.getName1(), scrapedTeam.getUrl2(), scrapedTeam.getName2());
  }

  private long getTournamentId(String tournamentUrl) {
    return bracketClient.getTournamentByUrl(tournamentUrl).map(Tournament::getId).orElseThrow();
  }

  private Optional<Long> getMatchId(ScrapedMatch match, long tournamentId) {
    MatchType matchType = match.getMatchType();
    Stage stage = match.getStage();
    ScrapedTeam team1 = match.getTeam1();
    ScrapedTeam team2 = match.getTeam2();

    if (StringUtils.isEmpty(team1.getUrl1()) || StringUtils.isEmpty(team2.getUrl1())) {
      return Optional.empty();
    }

    if (match.getAlternativeTeam1() != null) {
      return getMatchId(tournamentId, matchType, stage, team1, match.getAlternativeTeam1(), true);

    } else if (match.getAlternativeTeam2() != null) {
      return getMatchId(tournamentId, matchType, stage, team2, match.getAlternativeTeam2(), true);

    } else {
      return getMatchId(tournamentId, matchType, stage, team1, team2, false);
    }
  }

  private Optional<Long> getMatchId(long tournamentId, MatchType matchType, Stage stage, ScrapedTeam team1, ScrapedTeam team2, boolean fromPreviousRound) {
    Optional<Long> team1Player1 = getPlayerId(team1.getUrl1());
    Optional<Long> team2Player1 = getPlayerId(team2.getUrl1());
    Optional<Long> team1Player2 = matchType == MatchType.DOUBLES ? getPlayerId(team1.getUrl2()) : Optional.empty();
    Optional<Long> team2Player2 = matchType == MatchType.DOUBLES ? getPlayerId(team2.getUrl2()) : Optional.empty();

    if (team1Player1.isEmpty() || team2Player1.isEmpty()) {
      return Optional.empty();
    }

    if (matchType == MatchType.DOUBLES && (team1Player2.isEmpty() || team2Player2.isEmpty())) {
      return Optional.empty();
    }

    MatchByPlayersRequest request = new MatchByPlayersRequest(tournamentId, stage == Stage.QUALIFIER,
        team1Player1.get(), team2Player1.get(), team1Player2.orElse(null), team2Player2.orElse(null));

    Optional<Long> matchId;
    if (fromPreviousRound) {
      matchId = bracketClient.getNextRoundMatchByPlayers(request).map(Match::getId);
    } else {
      matchId = bracketClient.getMatchByPlayers(request).map(Match::getId);
    }

    return matchId;
  }

  private Optional<Long> getPlayerId(String playerUrl) {
    PlayerByUrlResponse playerByUrlResponse = playerRegistryClient.getPlayerByUrl(playerUrl);
    if (playerByUrlResponse.getStatus() != PlayerByUrlStatus.EXISTS) {
      if (playerByUrlResponse.getStatus() == PlayerByUrlStatus.TEMP_FAILURE) {
        LOGGER.info("player-registry doesn't have a player {} with url: {}", playerByUrlResponse.getStatus(), playerUrl);
      } else {
        LOGGER.error("player-registry doesn't have a player {} with url: {}", playerByUrlResponse.getStatus(), playerUrl);
      }
      return Optional.empty();
    }

    return Optional.of(playerByUrlResponse.getPlayer().getId());
  }
}
