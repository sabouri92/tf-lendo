package com.mrsabouri.tennisfollow.orderofplay.scraper.service;

import java.time.LocalDate;
import java.time.ZoneId;

public interface ZoneIdService {
  ZoneId fromLocation(String location, LocalDate day);
}
