package com.mrsabouri.tennisfollow.orderofplay.scraper.job.impl;

import com.mrsabouri.tennisfollow.orderofplay.scraper.gateway.ScheduleExtractor;
import com.mrsabouri.tennisfollow.orderofplay.scraper.job.SchedulesJob;
import com.mrsabouri.tennisfollow.orderofplay.scraper.model.ScrapedCourtSchedule;
import com.mrsabouri.tennisfollow.orderofplay.scraper.model.ScrapedTournamentCourts;
import com.mrsabouri.tennisfollow.orderofplay.scraper.service.CourtSchedulePersistor;
import com.mrsabouri.tennisfollow.orderofplay.scraper.service.ScrapedScheduleToEntityConverter;
import com.mrsabouri.tennisfollow.orderofplay.scraper.service.ScrapedTournamentCourtsValidator;
import com.mrsabouri.tennisfollow.orderofplay.scraper.service.TournamentCourtsCleaner;
import com.mrsabouri.tennisfollow.orderofplay.service.persistence.entity.CourtScheduleEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class SchedulesJobImpl implements SchedulesJob {
  private final Logger LOGGER = LoggerFactory.getLogger(getClass());

  private ScheduleExtractor scheduleExtractor;
  private ScrapedTournamentCourtsValidator scrapedTournamentCourtsValidator;
  private ScrapedScheduleToEntityConverter scrapedScheduleToEntityConverter;
  private CourtSchedulePersistor courtSchedulePersistor;
  private TournamentCourtsCleaner tournamentCourtsCleaner;

  @Autowired
  public SchedulesJobImpl(ScheduleExtractor scheduleExtractor,
                          ScrapedTournamentCourtsValidator scrapedTournamentCourtsValidator,
                          ScrapedScheduleToEntityConverter scrapedScheduleToEntityConverter,
                          CourtSchedulePersistor courtSchedulePersistor,
                          TournamentCourtsCleaner tournamentCourtsCleaner) {
    this.scheduleExtractor = scheduleExtractor;
    this.scrapedTournamentCourtsValidator = scrapedTournamentCourtsValidator;
    this.scrapedScheduleToEntityConverter = scrapedScheduleToEntityConverter;
    this.courtSchedulePersistor = courtSchedulePersistor;
    this.tournamentCourtsCleaner = tournamentCourtsCleaner;
  }

  @Async
  @Override
  public void trigger() {
    List<ScrapedTournamentCourts> tournamentCourtsList = scheduleExtractor.getTournaments();
    LOGGER.info("Found {} scrapedTournamentCourts.", tournamentCourtsList.size());

    for (ScrapedTournamentCourts tournamentCourts : tournamentCourtsList) {
      try {
        LOGGER.info("Processing tournament with url: {}", tournamentCourts.getTournamentUrl());

        scrapedTournamentCourtsValidator.validate(tournamentCourts);
        LOGGER.info("Tournament Courts validated.");

        tournamentCourtsCleaner.cleanInvalidCourts(tournamentCourts);
        LOGGER.info("Cleaned invalid courts.");

      } catch (RuntimeException rex) {
        LOGGER.error("Unable to perform schedules job.", rex);
        continue;
      }

      for (ScrapedCourtSchedule courtSchedule : tournamentCourts.getCourtSchedules()) {
        try {
          LOGGER.info("Processing court schedule item. {}", courtSchedule);

          CourtScheduleEntity courtScheduleEntity = scrapedScheduleToEntityConverter.convert(tournamentCourts, courtSchedule);
          LOGGER.info("Converted scraped DTOs to entities.");

          courtSchedulePersistor.persist(courtScheduleEntity);
          LOGGER.info("Persisted entities.");

        } catch (RuntimeException rex) {
          LOGGER.error("Unable to perform schedules job.", rex);
        }
      }
    }
  }
}
