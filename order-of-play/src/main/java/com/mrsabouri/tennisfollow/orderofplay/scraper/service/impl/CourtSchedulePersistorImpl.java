package com.mrsabouri.tennisfollow.orderofplay.scraper.service.impl;

import com.mrsabouri.tennisfollow.orderofplay.scraper.persistence.repository.UnknownMatchRepository;
import com.mrsabouri.tennisfollow.orderofplay.scraper.service.CourtSchedulePersistor;
import com.mrsabouri.tennisfollow.orderofplay.service.persistence.entity.CourtScheduleEntity;
import com.mrsabouri.tennisfollow.orderofplay.service.persistence.entity.MatchEntity;
import com.mrsabouri.tennisfollow.orderofplay.service.persistence.repository.CourtScheduleRepository;
import com.mrsabouri.tennisfollow.orderofplay.service.persistence.repository.MatchRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class CourtSchedulePersistorImpl implements CourtSchedulePersistor {
  private CourtScheduleRepository courtScheduleRepository;
  private MatchRepository matchRepository;
  private UnknownMatchRepository unknownMatchRepository;

  @Autowired
  public CourtSchedulePersistorImpl(CourtScheduleRepository courtScheduleRepository, MatchRepository matchRepository, UnknownMatchRepository unknownMatchRepository) {
    this.courtScheduleRepository = courtScheduleRepository;
    this.matchRepository = matchRepository;
    this.unknownMatchRepository = unknownMatchRepository;
  }

  @Override
  @Transactional
  public void persist(CourtScheduleEntity courtScheduleEntity) {
    Optional<CourtScheduleEntity> existingEntityOptional = courtScheduleRepository
        .findByTournamentIdAndDayAndCourtName(courtScheduleEntity.getTournamentId(), courtScheduleEntity.getDay(), courtScheduleEntity.getCourtName());

    // TODO check if needed
    if (existingEntityOptional.isPresent()) {
      CourtScheduleEntity existingEntity = existingEntityOptional.get();
      matchRepository.deleteByCourtScheduleAndIndexGreaterThanEqual(existingEntity, courtScheduleEntity.getMatches().size());
    }

    // Delete unknown matches entities if matchId is now available
    List<MatchEntity> matchesNoLongerUnknown = courtScheduleEntity.getMatches()
        .stream()
        .filter(matchEntity -> matchEntity.getMatchId() != null)
        .filter(matchEntity -> matchEntity.getUnknownMatch() != null)
        .filter(matchEntity -> matchEntity.getUnknownMatch().getId() != null)
        .collect(Collectors.toList());

    matchesNoLongerUnknown.stream()
        .map(MatchEntity::getUnknownMatch)
        .forEach(unknownMatchRepository::delete);

    matchesNoLongerUnknown.forEach(matchEntity -> matchEntity.setUnknownMatch(null));

    // Persist them all!
    courtScheduleRepository.save(courtScheduleEntity);
  }
}
