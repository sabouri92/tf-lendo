package com.mrsabouri.tennisfollow.orderofplay.service.model;

import java.time.LocalDate;
import java.util.List;

public class ScheduleDay {
  private LocalDate day;
  private List<CourtSchedule> courtSchedules;

  public ScheduleDay(LocalDate day) {
    this.day = day;
  }

  public ScheduleDay(LocalDate day, List<CourtSchedule> courtSchedules) {
    this.day = day;
    this.courtSchedules = courtSchedules;
  }

  public LocalDate getDay() {
    return day;
  }

  public List<CourtSchedule> getCourtSchedules() {
    return courtSchedules;
  }
}
