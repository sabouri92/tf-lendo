package com.mrsabouri.tennisfollow.orderofplay.scraper.service;

import com.mrsabouri.tennisfollow.orderofplay.service.persistence.entity.CourtScheduleEntity;

public interface CourtSchedulePersistor {
  void persist(CourtScheduleEntity courtScheduleEntity);
}
