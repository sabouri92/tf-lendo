package com.mrsabouri.tennisfollow.orderofplay.scraper.persistence.entity;

import com.mrsabouri.tennisfollow.orderofplay.enums.MatchType;
import com.mrsabouri.tennisfollow.orderofplay.service.persistence.entity.MatchEntity;

import javax.persistence.*;

@Table(name = "unknown_match")
@Entity
public class UnknownMatchEntity {
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE)
  private Long id;

  @OneToOne(optional = false)
  @JoinColumn(unique = true)
  private MatchEntity match;

  @Column(nullable = false)
  @Enumerated(EnumType.STRING)
  private MatchType matchType;

  @Embedded
  private UnknownTeam team1;

  @Embedded
  private UnknownTeam team2;

  @Embedded
  private UnknownTeam alternativeTeam1;

  @Embedded
  private UnknownTeam alternativeTeam2;

  public UnknownMatchEntity() {
  }

  public UnknownMatchEntity(MatchEntity match, MatchType matchType, UnknownTeam team1, UnknownTeam team2, UnknownTeam alternativeTeam1, UnknownTeam alternativeTeam2) {
    this.match = match;
    this.matchType = matchType;
    this.team1 = team1;
    this.team2 = team2;
    this.alternativeTeam1 = alternativeTeam1;
    this.alternativeTeam2 = alternativeTeam2;
  }

  public Long getId() {
    return id;
  }

  public MatchEntity getMatch() {
    return match;
  }

  public MatchType getMatchType() {
    return matchType;
  }

  public void setMatchType(MatchType matchType) {
    this.matchType = matchType;
  }

  public UnknownTeam getTeam1() {
    return team1;
  }

  public void setTeam1(UnknownTeam team1) {
    this.team1 = team1;
  }

  public UnknownTeam getTeam2() {
    return team2;
  }

  public void setTeam2(UnknownTeam team2) {
    this.team2 = team2;
  }

  public UnknownTeam getAlternativeTeam1() {
    return alternativeTeam1;
  }

  public void setAlternativeTeam1(UnknownTeam alternativeTeam1) {
    this.alternativeTeam1 = alternativeTeam1;
  }

  public UnknownTeam getAlternativeTeam2() {
    return alternativeTeam2;
  }

  public void setAlternativeTeam2(UnknownTeam alternativeTeam2) {
    this.alternativeTeam2 = alternativeTeam2;
  }
}
