package com.mrsabouri.tennisfollow.orderofplay.scraper.service;

import com.mrsabouri.tennisfollow.orderofplay.scraper.model.ScrapedTournamentCourts;

public interface ScrapedTournamentCourtsValidator {
  void validate(ScrapedTournamentCourts tournamentCourts);
}
