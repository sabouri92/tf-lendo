package com.mrsabouri.tennisfollow.orderofplay.scraper.service;

import com.mrsabouri.tennisfollow.orderofplay.scraper.model.ScrapedTournamentCourts;

public interface TournamentCourtsCleaner {
  void cleanInvalidCourts(ScrapedTournamentCourts tournamentCourts);
}
