package com.mrsabouri.tennisfollow.orderofplay.service.persistence.repository;

import com.mrsabouri.tennisfollow.orderofplay.service.persistence.entity.CourtScheduleEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface CourtScheduleRepository extends JpaRepository<CourtScheduleEntity, Long> {
  List<CourtScheduleEntity> findByTournamentIdAndDay(long tournamentId, LocalDate day);

  Optional<CourtScheduleEntity> findByTournamentIdAndDayAndCourtName(long tournamentId, LocalDate day, String courtName);

  List<CourtScheduleEntity> findByDay(LocalDate day);

  @Query("select distinct cs.day from CourtScheduleEntity cs order by cs.day desc")
  List<LocalDate> allDays();
}
