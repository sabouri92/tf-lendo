package com.mrsabouri.tennisfollow.orderofplay.service.persistence.entity;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Table(
    name = "court_schedule",
    uniqueConstraints = @UniqueConstraint(columnNames = {"tournamentId", "day", "courtName"})
)
@Entity
public class CourtScheduleEntity {
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE)
  private Long id;

  @Column(nullable = false)
  private long tournamentId;

  @Column(nullable = false)
  private LocalDate day;

  @Column(nullable = false)
  private String courtName;

  @OneToMany(mappedBy = "courtSchedule", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
  @OrderBy("index ASC")
  private List<MatchEntity> matches;

  public CourtScheduleEntity() {
  }

  public CourtScheduleEntity(long tournamentId, LocalDate day, String courtName) {
    this.tournamentId = tournamentId;
    this.day = day;
    this.courtName = courtName;
  }

  public static CourtScheduleEntity of(long tournamentId, LocalDate day, String courtName) {
    return new CourtScheduleEntity(tournamentId, day, courtName);
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public long getTournamentId() {
    return tournamentId;
  }

  public LocalDate getDay() {
    return day;
  }

  public String getCourtName() {
    return courtName;
  }

  public List<MatchEntity> getMatches() {
    return matches;
  }

  public void setMatches(List<MatchEntity> matches) {
    this.matches = matches;
  }

  public void addMatch(MatchEntity matchEntity) {
    if (matches == null) {
      matches = new LinkedList<>();
    }
    matches.add(matchEntity);
  }
}
