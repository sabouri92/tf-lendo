package com.mrsabouri.tennisfollow.orderofplay.rest;

import com.mrsabouri.tennisfollow.orderofplay.scraper.job.SchedulesJob;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class JobController {
  private SchedulesJob schedulesJob;

  @Autowired
  public JobController(SchedulesJob schedulesJob) {
    this.schedulesJob = schedulesJob;
  }

  @GetMapping("/job/schedules")
  public String triggerSchedulesJob() {
    schedulesJob.trigger();
    return "OK";
  }
}
