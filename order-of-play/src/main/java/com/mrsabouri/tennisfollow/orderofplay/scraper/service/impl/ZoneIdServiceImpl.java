package com.mrsabouri.tennisfollow.orderofplay.scraper.service.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mrsabouri.tennisfollow.orderofplay.scraper.service.ZoneIdService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Map;

@Component
public class ZoneIdServiceImpl implements ZoneIdService {
  private static final String GEO_CODE_API = "https://maps.googleapis.com/maps/api/geocode/json?address={address}&key={apiKey}";
  private static final String TIME_ZONE_API = "https://maps.googleapis.com/maps/api/timezone/json?location={location}&timestamp={timestamp}&key={apiKey}";

  @Value("${order-of-play.google.api-key}")
  private String apiKey;

  private ObjectMapper objectMapper;
  private RestTemplate restTemplate;

  @Autowired
  public ZoneIdServiceImpl(ObjectMapper objectMapper, RestTemplate restTemplate) {
    this.objectMapper = objectMapper;
    this.restTemplate = restTemplate;
  }

  @Override
  @Cacheable("order-of-play/zone-id")
  public ZoneId fromLocation(String location, LocalDate day) {
    try {
      long timestamp = day.toEpochSecond(LocalTime.of(0, 0), ZoneOffset.ofHours(0));

      String geoCodeJson = restTemplate.getForObject(GEO_CODE_API, String.class, Map.of("address", location, "apiKey", apiKey));
      String coordinates = coordinatesFromGeoCodeJson(geoCodeJson);

      String timeZoneJson = restTemplate.getForObject(TIME_ZONE_API, String.class, Map.of("location", coordinates, "timestamp", timestamp, "apiKey", apiKey));
      return zoneIdFromTimeZoneJson(timeZoneJson);

    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private String coordinatesFromGeoCodeJson(String geoCodeJson) throws IOException {
    JsonNode geoCodeNode = objectMapper.readTree(geoCodeJson);

    JsonNode resultsArray = geoCodeNode.get("results");
    Validate.isTrue(resultsArray.size() > 0);

    JsonNode geometry = resultsArray.get(0).get("geometry");
    JsonNode location = geometry.get("location");

    String lat = location.get("lat").toString();
    String lng = location.get("lng").toString();

    return lat + "," + lng;
  }

  private ZoneId zoneIdFromTimeZoneJson(String timeZoneJson) throws IOException {
    JsonNode timeZoneNode = objectMapper.readTree(timeZoneJson);
    String timeZoneId = timeZoneNode.get("timeZoneId").toString();
    timeZoneId = StringUtils.removeStart(timeZoneId, "\"");
    timeZoneId = StringUtils.removeEnd(timeZoneId, "\"");
    return ZoneId.of(timeZoneId);
  }
}
