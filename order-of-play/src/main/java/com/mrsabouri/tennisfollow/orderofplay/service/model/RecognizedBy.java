package com.mrsabouri.tennisfollow.orderofplay.service.model;

public enum RecognizedBy {
  MATCH_ID,
  PLAYER_NAMES
}
