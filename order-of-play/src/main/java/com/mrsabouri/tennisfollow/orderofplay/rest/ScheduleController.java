package com.mrsabouri.tennisfollow.orderofplay.rest;

import com.mrsabouri.tennisfollow.orderofplay.service.ScheduleService;
import com.mrsabouri.tennisfollow.orderofplay.service.model.ScheduleDay;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.List;

@RestController
public class ScheduleController {
  private ScheduleService scheduleService;

  @Autowired
  public ScheduleController(ScheduleService scheduleService) {
    this.scheduleService = scheduleService;
  }

  @GetMapping("/schedule/days")
  public List<ScheduleDay> scheduleDays() {
    return scheduleService.getScheduleDays();
  }

  @GetMapping("/schedule/day/{day}")
  public ScheduleDay scheduleDay(@PathVariable("day") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate day) {
    return scheduleService.scheduleForDay(day);
  }
}
