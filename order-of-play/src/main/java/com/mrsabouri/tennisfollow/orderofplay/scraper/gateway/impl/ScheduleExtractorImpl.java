package com.mrsabouri.tennisfollow.orderofplay.scraper.gateway.impl;

import com.mrsabouri.lib.jsouputils.DocumentLoader;
import com.mrsabouri.lib.jsouputils.ElementResolver;
import com.mrsabouri.tennisfollow.orderofplay.enums.MatchType;
import com.mrsabouri.tennisfollow.orderofplay.enums.Stage;
import com.mrsabouri.tennisfollow.orderofplay.scraper.gateway.ScheduleExtractor;
import com.mrsabouri.tennisfollow.orderofplay.scraper.model.ScrapedCourtSchedule;
import com.mrsabouri.tennisfollow.orderofplay.scraper.model.ScrapedMatch;
import com.mrsabouri.tennisfollow.orderofplay.scraper.model.ScrapedTeam;
import com.mrsabouri.tennisfollow.orderofplay.scraper.model.ScrapedTournamentCourts;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Component
public class ScheduleExtractorImpl implements ScheduleExtractor {
  private final Logger LOGGER = LoggerFactory.getLogger(getClass());

  private static final Predicate<String> TOURNAMENT_URL_PREDICATE = Pattern.compile("/en/scores/current/[^/]+/[0-9]+/daily-schedule").asPredicate();

  private static final List<String> QUALIFIERS_ROUNDS = List.of("Q1", "Q2", "Q3", "Q-1st", "Q-2nd", "Q-F");
  private static final List<String> MAIN_DRAW_ROUNDS = List.of("R1", "R2", "R3", "R4", "QF", "SF", "F", "1st Rd", "2nd Rd", "3rd Rd", "4th Rd");
  private static final List<String> ROUND_ROBIN_ROUNDS = List.of("RR");

  private static final String TOURNAMENTS_URL = "https://www.atptour.com/en/tournaments";
  private static final String ATP_WEBSITE_PREFIX = "https://www.atptour.com";

  private DocumentLoader documentLoader;
  private DateTimeFormatter scheduleDayFormat;
  private DateTimeFormatter startTimeFormat;

  @Autowired
  public ScheduleExtractorImpl(DocumentLoader documentLoader) {
    this.documentLoader = documentLoader;
    scheduleDayFormat = DateTimeFormatter.ofPattern("EEEE, MMMM d, y");
    startTimeFormat = DateTimeFormatter.ofPattern("h:m a");
  }

  @Override
  public List<ScrapedTournamentCourts> getTournaments() {
    Document tournamentsPage = documentLoader.getByUrl(TOURNAMENTS_URL);
    Elements tournamentElements = tournamentsPage.getElementsByAttributeValue("class", "tourney-result");

    List<ScrapedTournamentCourts> result = new LinkedList<>();

    for (Element tournamentElement : tournamentElements) {
      Elements scheduleElements = tournamentElement.getElementsByAttributeValue("data-ga-label", "Schedule");
      if (scheduleElements.isEmpty()) {
        continue;
      }

      try {
        Element scheduleElement = ElementResolver.resolveSingleElement("a", scheduleElements);
        String tournamentUrl = ATP_WEBSITE_PREFIX + ElementResolver.resolveSingleElementAttribute("a", "href", tournamentElement.getElementsByAttributeValue("class", "tourney-title"));

        String href = scheduleElement.attr("href");
        Validate.isTrue(TOURNAMENT_URL_PREDICATE.test(href));
        String schedulesUrl = ATP_WEBSITE_PREFIX + href;

        result.addAll(processTournament(schedulesUrl, tournamentUrl));

      } catch (RuntimeException rex) {
        LOGGER.error("Unable to extract schedules for tournament: {}", scheduleElements.html(), rex);
      }
    }

    return result;
  }

  private List<ScrapedTournamentCourts> processTournament(String schedulesUrl, String tournamentUrl) {
    Document mainPage = documentLoader.getByUrl(schedulesUrl);
    Element dayDropDownElement = ElementResolver.resolveSingleElement("ul", mainPage.getElementsByAttributeValue("data-value", "day"));

    List<Integer> days = dayDropDownElement.children()
        .stream()
        .map(element -> element.attr("data-value"))
        .map(Integer::parseInt)
        .collect(Collectors.toList());

    List<ScrapedTournamentCourts> tournamentCourtsList = new LinkedList<>();

    for (int day : days) {
      Document dayPage = documentLoader.getByUrl(String.format("%s?day=%d", schedulesUrl, day));
      List<Element> tourElements = dayPage.getElementsByAttributeValue("class", "tourney-results-wrapper");
      Validate.isTrue(tourElements.size() == 1);

      String location = ElementResolver.resolveSingleElementText("span", tourElements.get(0).getElementsByAttributeValue("class", "tourney-location"));
      String calendarDayText = ElementResolver.resolveSingleElementText("h3", dayPage.getElementsByAttributeValue("class", "day-table-date"));
      LocalDate calendarDay = LocalDate.from(scheduleDayFormat.parse(calendarDayText));
      List<ScrapedCourtSchedule> courtSchedules = extractCourtSchedules(dayPage);

      ScrapedTournamentCourts tournamentCourts = new ScrapedTournamentCourts(
          tournamentUrl,
          location,
          calendarDay,
          courtSchedules
      );
      tournamentCourtsList.add(tournamentCourts);
    }

    return tournamentCourtsList;
  }

  private List<ScrapedCourtSchedule> extractCourtSchedules(Document dayPage) {
    Elements dayTables = dayPage.getElementsByAttributeValue("class", "day-table");

    List<ScrapedCourtSchedule> courtSchedules = new LinkedList<>();

    for (Element dayTable : dayTables) {
      List<Element> rows = dayTable.getElementsByTag("tr");
      Validate.isTrue(rows.size() % 2 == 0);

      String courtName = getCourtName(rows.get(0));

      List<ScrapedMatch> scrapedMatches = getMatches(rows);

      ScrapedCourtSchedule courtSchedule = new ScrapedCourtSchedule(
          courtName,
          scrapedMatches
      );
      courtSchedules.add(courtSchedule);
    }

    return courtSchedules;
  }

  private String getCourtName(Element headerRow) {
    Validate.isTrue("tr".equals(headerRow.tagName()));
    Validate.isTrue("th".equals(headerRow.child(0).tagName()));
    Validate.isTrue("span".equals(headerRow.child(0).child(0).tagName()));
    return ElementResolver.resolveSingleElementText("span", headerRow.getElementsByAttributeValue("class", "day-table-heading"));
  }

  private List<ScrapedMatch> getMatches(List<Element> rows) {
    List<ScrapedMatch> scrapedMatches = new LinkedList<>();

    LocalTime notBefore = null;

    for (int i = 0; i < rows.size(); i++) {
      if (i%2 == 0) {
        notBefore = parseNotBefore(rows.get(i).child(0));
        continue;
      }

      List<Element> columns = rows.get(i).children();

      Stage stage = parseStage(columns.get(0));
      Element team1Element = columns.get(3);
      Element team2Element = columns.get(7);

      MatchType matchType = parseMatchType(team1Element);
      Validate.isTrue(matchType == parseMatchType(team2Element));

      ScrapedTeam[] team1Alternatives = getTeams(team1Element);
      ScrapedTeam[] team2Alternatives = getTeams(team2Element);

      ScrapedMatch scrapedMatch = new ScrapedMatch(
          notBefore, matchType, stage, team1Alternatives[0], team2Alternatives[0], team1Alternatives[1], team2Alternatives[1]
      );
      scrapedMatches.add(scrapedMatch);
    }

    return scrapedMatches;
  }

  private LocalTime parseNotBefore(Element element) {
    Node node = element.childNode(element.childNodeSize() - 1);
    Validate.isInstanceOf(TextNode.class, node);

    TextNode textNode = (TextNode) node;
    String text = textNode.getWholeText().replaceAll("\u00a0", "").trim();

    if (text.contains("/")) {
      text = text.substring(0, text.indexOf("/")).trim();
    }

    if (text.equalsIgnoreCase("followed by")) {
      return null;
    }

    text = StringUtils.removeStartIgnoreCase(text, "Starts at").trim();
    text = StringUtils.removeStartIgnoreCase(text, "Not Before").trim();

    if (text.toLowerCase().endsWith("noon")) {
      text = text.substring(0, text.toLowerCase().lastIndexOf("noon")) + "PM";
    }

    return LocalTime.from(startTimeFormat.parse(text.toUpperCase()));
  }

  private Stage parseStage(Element element) {
    String text = ElementResolver.getText(element);

    if (inIgnoreCase(text, MAIN_DRAW_ROUNDS)) {
      return Stage.MAIN_DRAW;
    } else if (inIgnoreCase(text, QUALIFIERS_ROUNDS)) {
      return Stage.QUALIFIER;
    } else if (inIgnoreCase(text, ROUND_ROBIN_ROUNDS)) {
      return Stage.ROUND_ROBIN;
    } else {
      return null;
    }
  }

  private boolean inIgnoreCase(String text, List<String> candidates) {
    return candidates.stream().anyMatch(candidate -> candidate.equalsIgnoreCase(text));
  }

  private MatchType parseMatchType(Element teamElement) {
    if (teamElement.child(0).tagName().equals("div")) {
      MatchType type = parseMatchType(teamElement.child(0));

      if (teamElement.children().size() == 2) {
        Validate.isTrue(type == parseMatchType(teamElement.child(1)));
      }

      return type;
    }

    List<Element> elements = teamElement.children();
    Validate.isTrue(elements.size() >= 1 && elements.size() <= 2);
    elements.forEach(element -> Validate.isTrue(element.tagName().equals("a")));

    return elements.size() == 1 ? MatchType.SINGLES : MatchType.DOUBLES;
  }

  private ScrapedTeam[] getTeams(Element teamElement) {
    ScrapedTeam team1, team2;

    if (teamElement.child(0).tagName().equals("div") && teamElement.children().size() == 1) {
      return getTeams(teamElement.child(0));
    }
    else if (teamElement.child(0).tagName().equals("div")) {
      Validate.isTrue(teamElement.children().size() == 2);
      Validate.isTrue(teamElement.child(1).tagName().equals("div"));

      team1 = getTeam(teamElement.child(0));
      team2 = getTeam(teamElement.child(1));
    } else {
      team1 = getTeam(teamElement);
      team2 = null;
    }

    return new ScrapedTeam[] {team1, team2};
  }

  private ScrapedTeam getTeam(Element element) {
    List<Element> players = element.children();
    players.forEach(player -> Validate.isTrue(player.tagName().equals("a")));
    Validate.isTrue(players.size() >= 1 && players.size() <= 2);

    String url1 = getPlayerUrl(players.get(0).attr("href"));
    String name1 = ElementResolver.getText(players.get(0));

    if (players.size() == 1) {
      return ScrapedTeam.singles(url1, name1);
    }

    String url2 = getPlayerUrl(players.get(1).attr("href"));
    String name2 = ElementResolver.getText(players.get(1));

    return ScrapedTeam.doubles(url1, url2, name1, name2);
  }

  private String getPlayerUrl(String href) {
    return StringUtils.isEmpty(href) ? null : (ATP_WEBSITE_PREFIX + href);
  }
}
