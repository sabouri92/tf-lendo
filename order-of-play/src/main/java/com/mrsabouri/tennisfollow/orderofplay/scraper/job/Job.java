package com.mrsabouri.tennisfollow.orderofplay.scraper.job;

public interface Job {
  void trigger();
}
