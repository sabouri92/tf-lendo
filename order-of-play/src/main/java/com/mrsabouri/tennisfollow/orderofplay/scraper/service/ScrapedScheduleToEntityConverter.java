package com.mrsabouri.tennisfollow.orderofplay.scraper.service;

import com.mrsabouri.tennisfollow.orderofplay.scraper.model.ScrapedCourtSchedule;
import com.mrsabouri.tennisfollow.orderofplay.scraper.model.ScrapedTournamentCourts;
import com.mrsabouri.tennisfollow.orderofplay.service.persistence.entity.CourtScheduleEntity;

public interface ScrapedScheduleToEntityConverter {
  CourtScheduleEntity convert(ScrapedTournamentCourts tournamentCourts, ScrapedCourtSchedule scrapedCourtSchedule);
}
