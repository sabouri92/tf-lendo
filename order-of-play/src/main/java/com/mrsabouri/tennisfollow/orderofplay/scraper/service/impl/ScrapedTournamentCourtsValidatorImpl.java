package com.mrsabouri.tennisfollow.orderofplay.scraper.service.impl;

import com.mrsabouri.tennisfollow.orderofplay.enums.MatchType;
import com.mrsabouri.tennisfollow.orderofplay.scraper.model.ScrapedCourtSchedule;
import com.mrsabouri.tennisfollow.orderofplay.scraper.model.ScrapedMatch;
import com.mrsabouri.tennisfollow.orderofplay.scraper.model.ScrapedTeam;
import com.mrsabouri.tennisfollow.orderofplay.scraper.model.ScrapedTournamentCourts;
import com.mrsabouri.tennisfollow.orderofplay.scraper.service.ScrapedTournamentCourtsValidator;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalTime;

@Component
public class ScrapedTournamentCourtsValidatorImpl implements ScrapedTournamentCourtsValidator {
  @Override
  public void validate(ScrapedTournamentCourts tournamentCourts) {
    Validate.notEmpty(tournamentCourts.getTournamentUrl());
    Validate.notEmpty(tournamentCourts.getTournamentLocation());

    LocalDate day = tournamentCourts.getDay();
    Validate.notNull(day);
    Validate.isTrue(day.isBefore(LocalDate.now().plusDays(3)));
    Validate.isTrue(day.isAfter(LocalDate.now().minusDays(20)));

    for (ScrapedCourtSchedule courtSchedule : tournamentCourts.getCourtSchedules()) {
      Validate.notNull(courtSchedule);
      Validate.notEmpty(courtSchedule.getCourtName());
      Validate.notEmpty(courtSchedule.getScrapedMatches());

      LocalTime lastTime = null;
      for (ScrapedMatch scrapedMatch : courtSchedule.getScrapedMatches()) {
        if (scrapedMatch.getNotBefore() != null) {
          if (lastTime != null) {
            Validate.isTrue(lastTime.isBefore(scrapedMatch.getNotBefore()));
          }
          lastTime = scrapedMatch.getNotBefore();
        }
      }

      courtSchedule.getScrapedMatches().forEach(this::validateMatch);
    }

    long distinctCourtsCount = tournamentCourts.getCourtSchedules()
        .stream()
        .map(ScrapedCourtSchedule::getCourtName)
        .distinct()
        .count();
    Validate.isTrue(distinctCourtsCount == tournamentCourts.getCourtSchedules().size());
  }

  private void validateMatch(ScrapedMatch scrapedMatch) {
    Validate.notNull(scrapedMatch.getMatchType());

    Validate.notNull(scrapedMatch.getTeam1());
    Validate.notNull(scrapedMatch.getTeam2());

    validateTeam(scrapedMatch.getTeam1(), scrapedMatch.getMatchType());
    validateTeam(scrapedMatch.getTeam2(), scrapedMatch.getMatchType());

    if (scrapedMatch.getAlternativeTeam1() != null) {
      validateTeam(scrapedMatch.getAlternativeTeam1(), scrapedMatch.getMatchType());
    }

    if (scrapedMatch.getAlternativeTeam2() != null) {
      validateTeam(scrapedMatch.getAlternativeTeam2(), scrapedMatch.getMatchType());
    }
  }

  private void validateTeam(ScrapedTeam scrapedTeam, MatchType matchType) {
    // Player 1 should always exist
    Validate.isTrue(!StringUtils.isEmpty(scrapedTeam.getName1()) || !StringUtils.isEmpty(scrapedTeam.getUrl1()));

    // Either match is singles or player 2 exists
    Validate.isTrue(matchType == MatchType.SINGLES || !StringUtils.isEmpty(scrapedTeam.getName2()) || !StringUtils.isEmpty(scrapedTeam.getUrl2()));

    // Either match is doubles or player 2 doesn't exist
    Validate.isTrue(matchType == MatchType.DOUBLES || scrapedTeam.getUrl2() == null && scrapedTeam.getName2() == null);
  }
}
