package com.mrsabouri.tennisfollow.orderofplay;

import com.mrsabouri.tennisfollow.alarmclock.AlarmClockClient;
import com.mrsabouri.tennisfollow.alarmclock.model.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import javax.annotation.PostConstruct;
import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;

@Configuration
@Profile("!no-alarm-clock")
public class AlarmClockRegistration {
  private final Logger LOGGER = LoggerFactory.getLogger(getClass());

  private static final String APPLICATION_NAME_PREFIX = "order-of-play.";
  private static final int VERSION = 1;
  private static final String JOB_URL_PREFIX = "http://order-of-play/job/";

  private static final Instant SCHEDULES_JOB_START = ZonedDateTime.of(2019, 3, 8, 0, 0, 0, 0, ZoneId.of("UTC")).toInstant();
  private static final Duration SCHEDULES_JOB_FREQUENCY = Duration.ofMinutes(15);

  private AlarmClockClient alarmClockClient;

  @Autowired
  public AlarmClockRegistration(AlarmClockClient alarmClockClient) {
    this.alarmClockClient = alarmClockClient;
  }

  @PostConstruct
  public void register() {
    Task schedulesTask = createSchedulesTask();

    tryRegister(schedulesTask);
  }

  private Task createSchedulesTask() {
    return new Task(
        APPLICATION_NAME_PREFIX + "schedules",
        VERSION,
        JOB_URL_PREFIX + "schedules",
        SCHEDULES_JOB_START,
        SCHEDULES_JOB_FREQUENCY
    );
  }

  private void tryRegister(Task task) {
    try {
      alarmClockClient.registerTask(task);
      LOGGER.info("Successfully registered alarm-clock task: {}", task);

    } catch (RuntimeException rex) {
      LOGGER.error("Unable to register task! {}", task, rex);
    }
  }
}
