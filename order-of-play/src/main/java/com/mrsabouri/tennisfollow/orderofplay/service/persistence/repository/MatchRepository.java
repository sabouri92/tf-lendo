package com.mrsabouri.tennisfollow.orderofplay.service.persistence.repository;

import com.mrsabouri.tennisfollow.orderofplay.service.persistence.entity.CourtScheduleEntity;
import com.mrsabouri.tennisfollow.orderofplay.service.persistence.entity.MatchEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MatchRepository extends JpaRepository<MatchEntity, Long> {
  void deleteByCourtScheduleAndIndexGreaterThanEqual(CourtScheduleEntity courtSchedule, int index);
}
