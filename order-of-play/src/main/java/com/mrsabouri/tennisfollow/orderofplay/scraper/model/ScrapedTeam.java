package com.mrsabouri.tennisfollow.orderofplay.scraper.model;

import java.util.Objects;

public class ScrapedTeam {
  private String url1;
  private String url2;
  private String name1;
  private String name2;

  public ScrapedTeam(String url1, String url2, String name1, String name2) {
    this.url1 = url1;
    this.url2 = url2;
    this.name1 = name1;
    this.name2 = name2;
  }

  public static ScrapedTeam singles(String url, String name) {
    return new ScrapedTeam(url, null, name, null);
  }

  public static ScrapedTeam doubles(String url1, String url2, String name1, String name2) {
    return new ScrapedTeam(url1, url2, name1, name2);
  }

  public String getUrl1() {
    return url1;
  }

  public String getUrl2() {
    return url2;
  }

  public String getName1() {
    return name1;
  }

  public String getName2() {
    return name2;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    ScrapedTeam that = (ScrapedTeam) o;
    return Objects.equals(url1, that.url1) &&
        Objects.equals(url2, that.url2) &&
        Objects.equals(name1, that.name1) &&
        Objects.equals(name2, that.name2);
  }

  @Override
  public int hashCode() {
    return Objects.hash(url1, url2, name1, name2);
  }

  @Override
  public String toString() {
    return "ScrapedTeam{" +
        "url1='" + url1 + '\'' +
        ", url2='" + url2 + '\'' +
        ", name1='" + name1 + '\'' +
        ", name2='" + name2 + '\'' +
        '}';
  }
}
