package com.mrsabouri.tennisfollow.orderofplay.service.model;

public class Team {
  private String player1;
  private String player2;

  public Team(String player1) {
    this.player1 = player1;
  }

  public Team(String player1, String player2) {
    this.player1 = player1;
    this.player2 = player2;
  }

  public static Team singles(String player1) {
    return new Team(player1);
  }

  public static Team doubles(String player1, String player2) {
    return new Team(player1, player2);
  }

  public String getPlayer1() {
    return player1;
  }

  public String getPlayer2() {
    return player2;
  }
}
