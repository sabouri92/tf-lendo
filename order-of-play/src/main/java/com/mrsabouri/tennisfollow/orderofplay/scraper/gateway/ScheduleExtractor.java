package com.mrsabouri.tennisfollow.orderofplay.scraper.gateway;

import com.mrsabouri.tennisfollow.orderofplay.scraper.model.ScrapedTournamentCourts;

import java.util.List;

public interface ScheduleExtractor {
  List<ScrapedTournamentCourts> getTournaments();
}
