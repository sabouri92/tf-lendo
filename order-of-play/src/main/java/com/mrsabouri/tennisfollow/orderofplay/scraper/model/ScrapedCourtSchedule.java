package com.mrsabouri.tennisfollow.orderofplay.scraper.model;

import java.util.List;

public class ScrapedCourtSchedule {
  private String courtName;
  private List<ScrapedMatch> scrapedMatches;

  public ScrapedCourtSchedule(String courtName, List<ScrapedMatch> scrapedMatches) {
    this.courtName = courtName;
    this.scrapedMatches = scrapedMatches;
  }

  public String getCourtName() {
    return courtName;
  }

  public List<ScrapedMatch> getScrapedMatches() {
    return scrapedMatches;
  }

  @Override
  public String toString() {
    return "ScrapedCourtSchedule{" +
        "courtName='" + courtName + '\'' +
        ", scrapedMatches=" + scrapedMatches +
        '}';
  }
}
