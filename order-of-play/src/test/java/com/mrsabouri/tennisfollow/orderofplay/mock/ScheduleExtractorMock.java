package com.mrsabouri.tennisfollow.orderofplay.mock;

import com.mrsabouri.tennisfollow.orderofplay.enums.MatchType;
import com.mrsabouri.tennisfollow.orderofplay.enums.Stage;
import com.mrsabouri.tennisfollow.orderofplay.scraper.gateway.ScheduleExtractor;
import com.mrsabouri.tennisfollow.orderofplay.scraper.model.ScrapedCourtSchedule;
import com.mrsabouri.tennisfollow.orderofplay.scraper.model.ScrapedMatch;
import com.mrsabouri.tennisfollow.orderofplay.scraper.model.ScrapedTeam;
import com.mrsabouri.tennisfollow.orderofplay.scraper.model.ScrapedTournamentCourts;
import com.mrsabouri.tennisfollow.test.interfaces.Cleanable;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Primary
@Profile("!no-mock")
public class ScheduleExtractorMock implements Cleanable, ScheduleExtractor {
  private static final ScrapedCourtSchedule UNCHANGED_COURT_SCHEDULE_1 = new ScrapedCourtSchedule(
      "Court 1",
      List.of(
          new ScrapedMatch(
              LocalTime.of(11, 30),
              MatchType.SINGLES,
              Stage.MAIN_DRAW,
              ScrapedTeam.singles(PlayerRegistryMock.URL1, "Player 1"),
              ScrapedTeam.singles(PlayerRegistryMock.URL2, "Player 2")
          ),
          new ScrapedMatch(
              null,
              MatchType.DOUBLES,
              null,
              ScrapedTeam.doubles(null, null, "Player A", "Player B"),
              ScrapedTeam.doubles(null, null, "Player C", "Player D")
          ),
          new ScrapedMatch(
              null,
              MatchType.DOUBLES,
              Stage.MAIN_DRAW,
              ScrapedTeam.doubles(PlayerRegistryMock.URL3, PlayerRegistryMock.URL4, "Player 3", "Player 4"),
              ScrapedTeam.doubles(PlayerRegistryMock.URL5, PlayerRegistryMock.URL6, "Player 5", "Player 6")
          )
      )
  );

  private static final ScrapedCourtSchedule CHANGED_COURT_SCHEDULE_1 = new ScrapedCourtSchedule(
      "Court 1",
      List.of(
          new ScrapedMatch(
              LocalTime.of(12, 30),
              MatchType.DOUBLES,
              null,
              ScrapedTeam.doubles(null, null, "Player A2", "Player B2"),
              ScrapedTeam.doubles(null, null, "Player C2", "Player D2")
          ),
          new ScrapedMatch(
              null,
              MatchType.SINGLES,
              Stage.MAIN_DRAW,
              ScrapedTeam.singles(PlayerRegistryMock.URL1, "Player 1"),
              ScrapedTeam.singles(PlayerRegistryMock.URL2, "Player 2")
          ),
          new ScrapedMatch(
              null,
              MatchType.DOUBLES,
              Stage.QUALIFIER,
              ScrapedTeam.doubles(PlayerRegistryMock.URL3, PlayerRegistryMock.URL4, "Player 3", "Player 4"),
              ScrapedTeam.doubles(PlayerRegistryMock.URL7, PlayerRegistryMock.URL8, "Player 7", "Player 8")
          )
      )
  );

  private static final String TOURNAMENT_URL1 = BracketMock.URL1;
  private static final String TOURNAMENT_LOCATION1 = "Paris, France";
  private static final LocalDate DAY1 = LocalDate.now();

  private static final List<ScrapedTournamentCourts> UNCHANGED = List.of(
      new ScrapedTournamentCourts(
          TOURNAMENT_URL1, TOURNAMENT_LOCATION1, DAY1, List.of(UNCHANGED_COURT_SCHEDULE_1)
      )
  );

  private List<ScrapedTournamentCourts> toReturn = UNCHANGED;

  @Override
  public void clean() {
    toReturn = UNCHANGED;
  }

  @Override
  public List<ScrapedTournamentCourts> getTournaments() {
    return toReturn;
  }

  public List<ScrapedTournamentCourts> getToReturn() {
    return toReturn;
  }

  public void setToReturn(int matches, boolean changed) {
    ScrapedCourtSchedule baseScrapedCourtSchedule = changed ? CHANGED_COURT_SCHEDULE_1 : UNCHANGED_COURT_SCHEDULE_1;
    if (matches > baseScrapedCourtSchedule.getScrapedMatches().size()) {
      throw new IllegalArgumentException();
    }

    ScrapedCourtSchedule scrapedCourtSchedule = new ScrapedCourtSchedule(
        baseScrapedCourtSchedule.getCourtName(),
        baseScrapedCourtSchedule.getScrapedMatches().stream().limit(matches).collect(Collectors.toList())
    );
    toReturn = List.of(new ScrapedTournamentCourts(TOURNAMENT_URL1, TOURNAMENT_LOCATION1, DAY1, List.of(scrapedCourtSchedule)));
  }
}
