package com.mrsabouri.tennisfollow.orderofplay;

import com.mrsabouri.tennisfollow.orderofplay.scraper.persistence.repository.UnknownMatchRepository;
import com.mrsabouri.tennisfollow.orderofplay.service.persistence.repository.CourtScheduleRepository;
import com.mrsabouri.tennisfollow.orderofplay.service.persistence.repository.MatchRepository;
import com.mrsabouri.tennisfollow.orderofplayclient.OrderOfPlayClient;
import com.mrsabouri.tennisfollow.orderofplayclient.impl.OrderOfPlayClientImpl;
import com.mrsabouri.tennisfollow.test.groups.BuildTests;
import com.mrsabouri.tennisfollow.test.interfaces.Cleanable;
import org.junit.After;
import org.junit.Before;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@ActiveProfiles({"test", "no-alarm-clock"})
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Category(BuildTests.class)
public class OrderOfPlayApplicationTestContext {

  private static final String SCHEDULES = "/schedules";

  private static final String JOB_ENDPOINT = "/job";

  @LocalServerPort
  protected int port;

  @Autowired
  protected TestRestTemplate restTemplate;

  @Autowired
  protected CourtScheduleRepository courtScheduleRepository;

  @Autowired
  protected MatchRepository matchRepository;

  @Autowired
  protected UnknownMatchRepository unknownMatchRepository;

  protected OrderOfPlayClient orderOfPlayClient;

  @Autowired(required = false)
  private List<Cleanable> cleanables;

  protected void triggerScheduleJob() {
    triggerJob(SCHEDULES);
  }

  private void triggerJob(String jobName) {
    String url = String.format("http://localhost:%d%s/%s", port, JOB_ENDPOINT, jobName);
    restTemplate.getForObject(url, String.class);
  }

  @Before
  public void setup() {
    orderOfPlayClient = new OrderOfPlayClientImpl(new RestTemplate(), "http://localhost:" + port + "/");
  }

  @After
  public void cleanUp() {
    courtScheduleRepository.deleteAll();
    matchRepository.deleteAll();
    unknownMatchRepository.deleteAll();

    if (cleanables != null) {
      cleanables.forEach(Cleanable::clean);
    }
  }

}
