package com.mrsabouri.tennisfollow.orderofplay.mock;

import com.mrsabouri.tennisfollow.playerregistry.PlayerRegistryClient;
import com.mrsabouri.tennisfollow.playerregistry.model.Player;
import com.mrsabouri.tennisfollow.playerregistry.model.PlayerByUrlResponse;
import com.mrsabouri.tennisfollow.playerregistry.model.PlayerByUrlStatus;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Optional;
import java.util.UUID;

@Component
@Primary
public class PlayerRegistryMock implements PlayerRegistryClient {
  public static final String URL1 = UUID.randomUUID().toString();
  public static final String URL2 = UUID.randomUUID().toString();
  public static final String URL3 = UUID.randomUUID().toString();
  public static final String URL4 = UUID.randomUUID().toString();
  public static final String URL5 = UUID.randomUUID().toString();
  public static final String URL6 = UUID.randomUUID().toString();
  public static final String URL7 = UUID.randomUUID().toString();
  public static final String URL8 = UUID.randomUUID().toString();

  private static Map<String, Integer> ID_FOR_URL = Map.ofEntries(
      Map.entry(URL1, 1),
      Map.entry(URL2, 2),
      Map.entry(URL3, 3),
      Map.entry(URL4, 4),
      Map.entry(URL5, 5),
      Map.entry(URL6, 6),
      Map.entry(URL7, 7),
      Map.entry(URL8, 8)
  );

  @Override
  public PlayerByUrlResponse getPlayerByUrl(String url) {
    Player player = playerWithId(ID_FOR_URL.get(url));
    PlayerByUrlStatus status = player == null ? PlayerByUrlStatus.TEMP_FAILURE : PlayerByUrlStatus.EXISTS;

    return new PlayerByUrlResponse() {
      @Override
      public Player getPlayer() {
        return player;
      }

      @Override
      public PlayerByUrlStatus getStatus() {
        return status;
      }
    };
  }

  @Override
  public Optional<Player> playerById(long playerId) {
    return Optional.empty();
  }

  private Player playerWithId(Integer id) {
    if (id == null) {
      return null;
    } else {
      return new Player() {
        @Override
        public long getId() {
          return id;
        }
      };
    }
  }
}
