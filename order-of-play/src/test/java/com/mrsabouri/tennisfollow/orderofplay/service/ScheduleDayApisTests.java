package com.mrsabouri.tennisfollow.orderofplay.service;

import com.mrsabouri.tennisfollow.orderofplay.OrderOfPlayApplicationTestContext;
import com.mrsabouri.tennisfollow.orderofplay.enums.MatchType;
import com.mrsabouri.tennisfollow.orderofplay.scraper.persistence.entity.UnknownMatchEntity;
import com.mrsabouri.tennisfollow.orderofplay.scraper.persistence.entity.UnknownTeam;
import com.mrsabouri.tennisfollow.orderofplay.service.persistence.entity.CourtScheduleEntity;
import com.mrsabouri.tennisfollow.orderofplay.service.persistence.entity.MatchEntity;
import com.mrsabouri.tennisfollow.orderofplayclient.model.*;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.time.Instant;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static junit.framework.TestCase.*;

@SuppressWarnings("SameParameterValue")
public class ScheduleDayApisTests extends OrderOfPlayApplicationTestContext {
  private static final long TOURNAMENT_ID1 = 1;
  private static final long TOURNAMENT_ID2 = 2;

  private static final long MATCH_ID1 = 10;

  private static final String COURT_NAME1 = "Some court";
  private static final String COURT_NAME2 = "Another court";
  private static final String COURT_NAME3 = "Third court";

  private static final String PLAYER_NAME1 = "Some name";
  private static final String PLAYER_NAME2 = "Another name";
  private static final String PLAYER_NAME3 = "Third name";
  private static final String PLAYER_NAME4 = "Fourth name";
  private static final String PLAYER_NAME5 = "5th name";
  private static final String PLAYER_NAME6 = "6th name";


  @Test
  public void allDaysApiWithMultipleDays() {
    courtScheduleRepository.save(CourtScheduleEntity.of(TOURNAMENT_ID1, LocalDate.now(), COURT_NAME1));
    courtScheduleRepository.save(CourtScheduleEntity.of(TOURNAMENT_ID1, LocalDate.now().plusDays(2), COURT_NAME1));
    courtScheduleRepository.save(CourtScheduleEntity.of(TOURNAMENT_ID1, LocalDate.now().minusDays(2), COURT_NAME1));
    courtScheduleRepository.save(CourtScheduleEntity.of(TOURNAMENT_ID1, LocalDate.now(), COURT_NAME2));
    courtScheduleRepository.save(CourtScheduleEntity.of(TOURNAMENT_ID2, LocalDate.now(), COURT_NAME3));

    List<ScheduleDay> scheduleDays = orderOfPlayClient.scheduleDays();

    assertEquals(3, scheduleDays.size());
    assertEquals(LocalDate.now().plusDays(2), scheduleDays.get(0).getDay());
    assertEquals(LocalDate.now(), scheduleDays.get(1).getDay());
    assertEquals(LocalDate.now().minusDays(2), scheduleDays.get(2).getDay());
  }

  @Test
  public void singleDayChoosesRightDay() {
    // Create test data
    LocalDate day = LocalDate.now();

    CourtScheduleEntity court1, court2;

    courtScheduleRepository.save(CourtScheduleEntity.of(TOURNAMENT_ID1, day.plusDays(1), COURT_NAME1));
    courtScheduleRepository.save(CourtScheduleEntity.of(TOURNAMENT_ID1, day.plusDays(1), COURT_NAME2));
    courtScheduleRepository.save(court1 = CourtScheduleEntity.of(TOURNAMENT_ID1, day, COURT_NAME1));
    courtScheduleRepository.save(court2 = CourtScheduleEntity.of(TOURNAMENT_ID1, day, COURT_NAME2));
    courtScheduleRepository.save(CourtScheduleEntity.of(TOURNAMENT_ID1, day.minusDays(2), COURT_NAME1));

    // Call API
    ResponseEntity<ExpandedSchedule> response = restTemplate.getForEntity(
        "http://localhost:{port}/schedule/day/{day}",
        ExpandedSchedule.class,
        Map.of("port", port, "day", day)
    );

    // Verify response
    assertEquals(HttpStatus.OK, response.getStatusCode());
    assertNotNull(response.getBody());

    ExpandedSchedule scheduleDay = response.getBody();
    assertEquals(day, scheduleDay.getDay());
    assertEquals(2, scheduleDay.getCourtSchedules().size());

    Optional<CourtSchedule> courtSchedule1 = scheduleDay.getCourtSchedules().stream().filter(courtSchedule -> courtSchedule.getCourtName().equals(COURT_NAME1)).findFirst();
    assertTrue(courtSchedule1.isPresent());
    assertCourtSchedules(court1, courtSchedule1.get(), day);

    Optional<CourtSchedule> courtSchedule2 = scheduleDay.getCourtSchedules().stream().filter(courtSchedule -> courtSchedule.getCourtName().equals(COURT_NAME2)).findFirst();
    assertTrue(courtSchedule2.isPresent());
    assertCourtSchedules(court2, courtSchedule2.get(), day);
  }

  @Test
  public void singleDayApiVariedMatches() {
    // Create test data
    LocalDate day = LocalDate.now();

    CourtScheduleEntity court1 = CourtScheduleEntity.of(TOURNAMENT_ID1, day, COURT_NAME1);
    CourtScheduleEntity court2 = CourtScheduleEntity.of(TOURNAMENT_ID2, day, COURT_NAME2);

    List<MatchEntity> court1Matches = List.of(
        knownMatch(court1, 0, MATCH_ID1, Instant.now()),
        singlesUnknownMatch(court1, 1, PLAYER_NAME1, PLAYER_NAME2, null)
    );
    court1.setMatches(court1Matches);

    List<MatchEntity> court2Matches = List.of(
        doublesUnknownMatchWithAlternatives(court2, 0, PLAYER_NAME1, PLAYER_NAME2, PLAYER_NAME3, PLAYER_NAME4, PLAYER_NAME5, PLAYER_NAME6, null)
    );
    court2.setMatches(court2Matches);

    // Save test data
    courtScheduleRepository.save(court1);
    courtScheduleRepository.save(court2);

    // Call API
    ResponseEntity<ExpandedSchedule> response = restTemplate.getForEntity(
        "http://localhost:{port}/schedule/day/{day}",
        ExpandedSchedule.class,
        Map.of("port", port, "day", day)
    );

    // Verify response
    assertEquals(HttpStatus.OK, response.getStatusCode());
    assertNotNull(response.getBody());

    ExpandedSchedule scheduleDay = response.getBody();
    assertEquals(day, scheduleDay.getDay());
    assertEquals(2, scheduleDay.getCourtSchedules().size());

    Optional<CourtSchedule> courtSchedule1 = scheduleDay.getCourtSchedules().stream().filter(courtSchedule -> courtSchedule.getCourtName().equals(COURT_NAME1)).findFirst();
    assertTrue(courtSchedule1.isPresent());
    assertCourtSchedules(court1, courtSchedule1.get(), day);

    Optional<CourtSchedule> courtSchedule2 = scheduleDay.getCourtSchedules().stream().filter(courtSchedule -> courtSchedule.getCourtName().equals(COURT_NAME2)).findFirst();
    assertTrue(courtSchedule2.isPresent());
    assertCourtSchedules(court2, courtSchedule2.get(), day);
  }

  private void assertCourtSchedules(CourtScheduleEntity courtScheduleEntity, CourtSchedule courtSchedule, LocalDate day) {
    assertEquals(courtScheduleEntity.getTournamentId(), courtSchedule.getTournamentId());
    assertEquals(courtScheduleEntity.getCourtName(), courtSchedule.getCourtName());
    assertEquals(courtScheduleEntity.getDay(), day);

    if (courtScheduleEntity.getMatches() == null) {
      assertEquals(0, courtSchedule.getMatches().size());
      return;
    }
    assertEquals(courtScheduleEntity.getMatches().size(), courtSchedule.getMatches().size());

    for (int i = 0;i < courtScheduleEntity.getMatches().size();i++) {
      MatchEntity matchEntity = courtScheduleEntity.getMatches().get(i);
      Match match = courtSchedule.getMatches().get(i);

      assertEquals(matchEntity.getMatchId(), match.getMatchId());
      assertEquals(matchEntity.getNotBefore(), match.getNotBefore());

      if (matchEntity.getUnknownMatch() == null) {
        assertEquals(RecognizedBy.MATCH_ID, match.getRecognizedBy());
        assertNull(match.getTeam1());
        assertNull(match.getTeam2());
        assertNull(match.getAlternativeTeam1());
        assertNull(match.getAlternativeTeam2());
        assertNull(match.getMatchType());
      } else {
        UnknownMatchEntity unknownMatch = matchEntity.getUnknownMatch();

        assertEquals(RecognizedBy.PLAYER_NAMES, match.getRecognizedBy());
        assertTeams(unknownMatch.getTeam1(), match.getTeam1());
        assertTeams(unknownMatch.getTeam2(), match.getTeam2());
        assertTeams(unknownMatch.getAlternativeTeam1(), match.getAlternativeTeam1());
        assertTeams(unknownMatch.getAlternativeTeam2(), match.getAlternativeTeam2());
        assertEquals(unknownMatch.getMatchType().name(), match.getMatchType().name());
      }
    }
  }

  private void assertTeams(UnknownTeam unknownTeam, Team team) {
    if (team == null) {
      assertNull(unknownTeam.getName1());
      assertNull(unknownTeam.getName2());
    } else {
      assertEquals(unknownTeam.getName1(), team.getPlayer1());
      assertEquals(unknownTeam.getName2(), team.getPlayer2());
    }
  }

  private MatchEntity knownMatch(CourtScheduleEntity courtScheduleEntity, int index, long matchId, Instant notBefore) {
    MatchEntity matchEntity = new MatchEntity(courtScheduleEntity, index);
    matchEntity.setMatchId(matchId);
    matchEntity.setNotBefore(notBefore);
    return matchEntity;
  }

  private MatchEntity singlesUnknownMatch(CourtScheduleEntity courtScheduleEntity, int index, String playerName1, String playerName2, Instant notBefore) {
    MatchEntity matchEntity = new MatchEntity(courtScheduleEntity, index);

    UnknownMatchEntity unknownMatch = new UnknownMatchEntity(
        matchEntity,
        MatchType.SINGLES,
        new UnknownTeam(null, playerName1, null, null),
        new UnknownTeam(null, playerName2, null, null),
        new UnknownTeam(),
        new UnknownTeam()
    );

    matchEntity.setUnknownMatch(unknownMatch);
    matchEntity.setNotBefore(notBefore);
    return matchEntity;
  }

  private MatchEntity doublesUnknownMatchWithAlternatives(CourtScheduleEntity courtScheduleEntity, int index, String playerName1, String playerName2,
                                                          String playerName3, String playerName4, String playerName5, String playerName6, Instant notBefore) {
    MatchEntity matchEntity = new MatchEntity(courtScheduleEntity, index);

    UnknownMatchEntity unknownMatch = new UnknownMatchEntity(
        matchEntity,
        MatchType.DOUBLES,
        new UnknownTeam(null, playerName1, null, playerName2),
        new UnknownTeam(null, playerName3, null, playerName4),
        new UnknownTeam(null, playerName5, null, playerName6),
        new UnknownTeam()
    );

    matchEntity.setUnknownMatch(unknownMatch);
    matchEntity.setNotBefore(notBefore);
    return matchEntity;
  }
}
