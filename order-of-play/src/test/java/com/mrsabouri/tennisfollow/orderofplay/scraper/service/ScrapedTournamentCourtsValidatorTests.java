package com.mrsabouri.tennisfollow.orderofplay.scraper.service;

import com.mrsabouri.tennisfollow.orderofplay.UnitTestsContext;
import com.mrsabouri.tennisfollow.orderofplay.enums.MatchType;
import com.mrsabouri.tennisfollow.orderofplay.enums.Stage;
import com.mrsabouri.tennisfollow.orderofplay.scraper.model.ScrapedCourtSchedule;
import com.mrsabouri.tennisfollow.orderofplay.scraper.model.ScrapedMatch;
import com.mrsabouri.tennisfollow.orderofplay.scraper.model.ScrapedTeam;
import com.mrsabouri.tennisfollow.orderofplay.scraper.model.ScrapedTournamentCourts;
import com.mrsabouri.tennisfollow.orderofplay.scraper.service.impl.ScrapedTournamentCourtsValidatorImpl;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

public class ScrapedTournamentCourtsValidatorTests extends UnitTestsContext {
  private ScrapedTournamentCourtsValidator scrapedTournamentCourtsValidator;

  @Before
  public void setup() {
    scrapedTournamentCourtsValidator = new ScrapedTournamentCourtsValidatorImpl();
  }

  @Test
  public void validSimple() {
    ScrapedTournamentCourts tournamentCourts = new ScrapedTournamentCourts(
        "http://www.some-site.com/",
        "Stockholm, Sweden",
        LocalDate.now(),
        List.of(
            new ScrapedCourtSchedule(
                "Court Name",
                List.of(
                    new ScrapedMatch(
                        LocalTime.of(11, 30),
                        MatchType.SINGLES,
                        Stage.MAIN_DRAW,
                        ScrapedTeam.singles("some-url", "Name"),
                        ScrapedTeam.singles("another-url", "Name 2")
                    )
                )
            )
        )
    );

    scrapedTournamentCourtsValidator.validate(tournamentCourts);
  }

  @Test
  public void validMinimal() {
    ScrapedTournamentCourts tournamentCourts = new ScrapedTournamentCourts(
        "url",
        "Stockholm, Sweden",
        LocalDate.now(),
        List.of(
            new ScrapedCourtSchedule(
                "Court Name",
                List.of(
                    new ScrapedMatch(
                        null,
                        MatchType.SINGLES,
                        null,
                        ScrapedTeam.singles(null, "Name 1"),
                        ScrapedTeam.singles(null, "Name 2")
                    )
                )
            )
        )
    );

    scrapedTournamentCourtsValidator.validate(tournamentCourts);
  }

  @Test(expected = IllegalArgumentException.class)
  public void invalidNoUrl() {
    ScrapedTournamentCourts tournamentCourts = new ScrapedTournamentCourts(
        "",
        "Stockholm, Sweden",
        LocalDate.now(),
        List.of(
            new ScrapedCourtSchedule(
                "Court Name",
                List.of(
                    new ScrapedMatch(
                        null,
                        MatchType.SINGLES,
                        null,
                        ScrapedTeam.singles(null, "Name 1"),
                        ScrapedTeam.singles(null, "Name 2")
                    )
                )
            )
        )
    );

    scrapedTournamentCourtsValidator.validate(tournamentCourts);
  }

  @Test(expected = IllegalArgumentException.class)
  public void invalidNoLocation() {
    ScrapedTournamentCourts tournamentCourts = new ScrapedTournamentCourts(
        "url",
        "",
        LocalDate.now(),
        List.of(
            new ScrapedCourtSchedule(
                "Court Name",
                List.of(
                    new ScrapedMatch(
                        null,
                        MatchType.SINGLES,
                        null,
                        ScrapedTeam.singles(null, "Name 1"),
                        ScrapedTeam.singles(null, "Name 2")
                    )
                )
            )
        )
    );

    scrapedTournamentCourtsValidator.validate(tournamentCourts);
  }

  @Test(expected = NullPointerException.class)
  public void invalidNoDate() {
    ScrapedTournamentCourts tournamentCourts = new ScrapedTournamentCourts(
        "url",
        "Stockholm, Sweden",
        null,
        List.of(
            new ScrapedCourtSchedule(
                "Court Name",
                List.of(
                    new ScrapedMatch(
                        null,
                        MatchType.SINGLES,
                        null,
                        ScrapedTeam.singles(null, "Name 1"),
                        ScrapedTeam.singles(null, "Name 2")
                    )
                )
            )
        )
    );

    scrapedTournamentCourtsValidator.validate(tournamentCourts);
  }

  @Test(expected = IllegalArgumentException.class)
  public void invalidNoCourtName() {
    ScrapedTournamentCourts tournamentCourts = new ScrapedTournamentCourts(
        "url",
        "Stockholm, Sweden",
        LocalDate.now(),
        List.of(
            new ScrapedCourtSchedule(
                "",
                List.of(
                    new ScrapedMatch(
                        null,
                        MatchType.SINGLES,
                        null,
                        ScrapedTeam.singles(null, "Name 1"),
                        ScrapedTeam.singles(null, "Name 2")
                    )
                )
            )
        )
    );

    scrapedTournamentCourtsValidator.validate(tournamentCourts);
  }

  @Test(expected = NullPointerException.class)
  public void invalidNoMatchType() {
    ScrapedTournamentCourts tournamentCourts = new ScrapedTournamentCourts(
        "url",
        "Stockholm, Sweden",
        LocalDate.now(),
        List.of(
            new ScrapedCourtSchedule(
                "Court Name",
                List.of(
                    new ScrapedMatch(
                        null,
                        null,
                        null,
                        ScrapedTeam.singles(null, "Name 1"),
                        ScrapedTeam.singles(null, "Name 2")
                    )
                )
            )
        )
    );

    scrapedTournamentCourtsValidator.validate(tournamentCourts);
  }

  @Test(expected = IllegalArgumentException.class)
  public void invalidNoPlayer1Name() {
    ScrapedTournamentCourts tournamentCourts = new ScrapedTournamentCourts(
        "url",
        "Stockholm, Sweden",
        LocalDate.now(),
        List.of(
            new ScrapedCourtSchedule(
                "Court Name",
                List.of(
                    new ScrapedMatch(
                        null,
                        MatchType.SINGLES,
                        null,
                        ScrapedTeam.singles("", null),
                        ScrapedTeam.singles(null, "Name 2")
                    )
                )
            )
        )
    );

    scrapedTournamentCourtsValidator.validate(tournamentCourts);
  }

  @Test(expected = IllegalArgumentException.class)
  public void invalidNoPlayer2Name() {
    ScrapedTournamentCourts tournamentCourts = new ScrapedTournamentCourts(
        "url",
        "Stockholm, Sweden",
        LocalDate.now(),
        List.of(
            new ScrapedCourtSchedule(
                "Court Name",
                List.of(
                    new ScrapedMatch(
                        null,
                        MatchType.SINGLES,
                        null,
                        ScrapedTeam.singles(null, "Name 1"),
                        ScrapedTeam.singles(null, "")
                    )
                )
            )
        )
    );

    scrapedTournamentCourtsValidator.validate(tournamentCourts);
  }

  @Test
  public void validPlayerUrlOnly() {
    ScrapedTournamentCourts tournamentCourts = new ScrapedTournamentCourts(
        "url",
        "Stockholm, Sweden",
        LocalDate.now(),
        List.of(
            new ScrapedCourtSchedule(
                "Court Name",
                List.of(
                    new ScrapedMatch(
                        null,
                        MatchType.SINGLES,
                        null,
                        ScrapedTeam.singles("url1", null),
                        ScrapedTeam.singles(null, "Name")
                    )
                )
            )
        )
    );

    scrapedTournamentCourtsValidator.validate(tournamentCourts);
  }

  @Test(expected = IllegalArgumentException.class)
  public void invalidSinglesMatchType() {
    ScrapedTournamentCourts tournamentCourts = new ScrapedTournamentCourts(
        "http://www.some-site.com/",
        "Stockholm, Sweden",
        LocalDate.now(),
        List.of(
            new ScrapedCourtSchedule(
                "Court Name",
                List.of(
                    new ScrapedMatch(
                        LocalTime.of(11, 30),
                        MatchType.SINGLES,
                        Stage.MAIN_DRAW,
                        ScrapedTeam.singles("some-url", "Name"),
                        ScrapedTeam.doubles("another-url", "third-url", "Name 2", "Name 3")
                    )
                )
            )
        )
    );

    scrapedTournamentCourtsValidator.validate(tournamentCourts);
  }

  @Test(expected = IllegalArgumentException.class)
  public void invalidDoublesMatchType() {
    ScrapedTournamentCourts tournamentCourts = new ScrapedTournamentCourts(
        "http://www.some-site.com/",
        "Stockholm, Sweden",
        LocalDate.now(),
        List.of(
            new ScrapedCourtSchedule(
                "Court Name",
                List.of(
                    new ScrapedMatch(
                        LocalTime.of(11, 30),
                        MatchType.DOUBLES,
                        Stage.MAIN_DRAW,
                        ScrapedTeam.singles("some-url", "Name"),
                        ScrapedTeam.doubles("another-url", "third-url", "Name 2", "Name 3")
                    )
                )
            )
        )
    );

    scrapedTournamentCourtsValidator.validate(tournamentCourts);
  }

  @Test(expected = IllegalArgumentException.class)
  public void invalidDateTooMuchFuture() {
    ScrapedTournamentCourts tournamentCourts = new ScrapedTournamentCourts(
        "http://www.some-site.com/",
        "Stockholm, Sweden",
        LocalDate.now().plusDays(5),
        List.of(
            new ScrapedCourtSchedule(
                "Court Name",
                List.of(
                    new ScrapedMatch(
                        LocalTime.of(11, 30),
                        MatchType.SINGLES,
                        Stage.MAIN_DRAW,
                        ScrapedTeam.singles("some-url", "Name"),
                        ScrapedTeam.singles("another-url", "Name 2")
                    )
                )
            )
        )
    );

    scrapedTournamentCourtsValidator.validate(tournamentCourts);
  }

  @Test(expected = IllegalArgumentException.class)
  public void invalidDateTooMuchPast() {
    ScrapedTournamentCourts tournamentCourts = new ScrapedTournamentCourts(
        "http://www.some-site.com/",
        "Stockholm, Sweden",
        LocalDate.now().minusDays(25),
        List.of(
            new ScrapedCourtSchedule(
                "Court Name",
                List.of(
                    new ScrapedMatch(
                        LocalTime.of(11, 30),
                        MatchType.SINGLES,
                        Stage.MAIN_DRAW,
                        ScrapedTeam.singles("some-url", "Name"),
                        ScrapedTeam.singles("another-url", "Name 2")
                    )
                )
            )
        )
    );

    scrapedTournamentCourtsValidator.validate(tournamentCourts);
  }

  @Test(expected = IllegalArgumentException.class)
  public void invalidDuplicateCourtName() {
    ScrapedTournamentCourts tournamentCourts = new ScrapedTournamentCourts(
        "http://www.some-site.com/",
        "Stockholm, Sweden",
        LocalDate.now(),
        List.of(
            new ScrapedCourtSchedule(
                "Court Name",
                List.of(
                    new ScrapedMatch(
                        LocalTime.of(11, 30),
                        MatchType.SINGLES,
                        Stage.MAIN_DRAW,
                        ScrapedTeam.singles("some-url", "Name"),
                        ScrapedTeam.singles("another-url", "Name 2")
                    )
                )
            ),
            new ScrapedCourtSchedule(
                "Court Name",
                List.of(
                    new ScrapedMatch(
                        LocalTime.of(11, 30),
                        MatchType.SINGLES,
                        Stage.MAIN_DRAW,
                        ScrapedTeam.singles("some-url", "Name"),
                        ScrapedTeam.singles("another-url", "Name 2")
                    )
                )
            )
        )
    );

    scrapedTournamentCourtsValidator.validate(tournamentCourts);
  }

  @Test(expected = IllegalArgumentException.class)
  public void invalidOutOfOrderTimes() {
    ScrapedTournamentCourts tournamentCourts = new ScrapedTournamentCourts(
        "http://www.some-site.com/",
        "Stockholm, Sweden",
        LocalDate.now(),
        List.of(
            new ScrapedCourtSchedule(
                "Court Name",
                List.of(
                    new ScrapedMatch(
                        LocalTime.of(13, 30),
                        MatchType.SINGLES,
                        Stage.MAIN_DRAW,
                        ScrapedTeam.singles("some-url", "Name"),
                        ScrapedTeam.singles("another-url", "Name 2")
                    ),
                    new ScrapedMatch(
                        LocalTime.of(11, 30),
                        MatchType.SINGLES,
                        Stage.MAIN_DRAW,
                        ScrapedTeam.singles("some-url", "Name"),
                        ScrapedTeam.singles("another-url", "Name 2")
                    )
                )
            )
        )
    );

    scrapedTournamentCourtsValidator.validate(tournamentCourts);
  }

  @Test
  public void validAlternative1Null() {
    ScrapedTournamentCourts tournamentCourts = new ScrapedTournamentCourts(
        "http://www.some-site.com/",
        "Stockholm, Sweden",
        LocalDate.now(),
        List.of(
            new ScrapedCourtSchedule(
                "Court Name",
                List.of(
                    new ScrapedMatch(
                        LocalTime.of(11, 30),
                        MatchType.SINGLES,
                        Stage.MAIN_DRAW,
                        ScrapedTeam.singles("some-url", "Name"),
                        ScrapedTeam.singles("another-url", "Name 2"),
                        null,
                        ScrapedTeam.singles(null, "Name 3")
                    )
                )
            )
        )
    );

    scrapedTournamentCourtsValidator.validate(tournamentCourts);
  }

  @Test
  public void validAlternative2Null() {
    ScrapedTournamentCourts tournamentCourts = new ScrapedTournamentCourts(
        "http://www.some-site.com/",
        "Stockholm, Sweden",
        LocalDate.now(),
        List.of(
            new ScrapedCourtSchedule(
                "Court Name",
                List.of(
                    new ScrapedMatch(
                        LocalTime.of(11, 30),
                        MatchType.SINGLES,
                        Stage.MAIN_DRAW,
                        ScrapedTeam.singles("some-url", "Name"),
                        ScrapedTeam.singles("another-url", "Name 2"),
                        ScrapedTeam.singles("third-url", null),
                        null
                    )
                )
            )
        )
    );

    scrapedTournamentCourtsValidator.validate(tournamentCourts);
  }

  @Test
  public void validAlternatives() {
    ScrapedTournamentCourts tournamentCourts = new ScrapedTournamentCourts(
        "http://www.some-site.com/",
        "Stockholm, Sweden",
        LocalDate.now(),
        List.of(
            new ScrapedCourtSchedule(
                "Court Name",
                List.of(
                    new ScrapedMatch(
                        LocalTime.of(11, 30),
                        MatchType.SINGLES,
                        Stage.MAIN_DRAW,
                        ScrapedTeam.singles("some-url", "Name"),
                        ScrapedTeam.singles("another-url", "Name 2"),
                        ScrapedTeam.singles("third-url", "Name 3"),
                        ScrapedTeam.singles("fourth-url", "Name 4")
                    )
                )
            )
        )
    );

    scrapedTournamentCourtsValidator.validate(tournamentCourts);
  }

  @Test(expected = IllegalArgumentException.class)
  public void invalidAlternative1MatchType() {
    ScrapedTournamentCourts tournamentCourts = new ScrapedTournamentCourts(
        "http://www.some-site.com/",
        "Stockholm, Sweden",
        LocalDate.now(),
        List.of(
            new ScrapedCourtSchedule(
                "Court Name",
                List.of(
                    new ScrapedMatch(
                        LocalTime.of(11, 30),
                        MatchType.SINGLES,
                        Stage.MAIN_DRAW,
                        ScrapedTeam.singles("some-url", "Name"),
                        ScrapedTeam.singles("another-url", "Name 2"),
                        ScrapedTeam.doubles("third-url", "fourth-url", "Name 3", "Name 4"),
                        null
                    )
                )
            )
        )
    );

    scrapedTournamentCourtsValidator.validate(tournamentCourts);
  }

  @Test(expected = IllegalArgumentException.class)
  public void invalidAlternative2MatchType() {
    ScrapedTournamentCourts tournamentCourts = new ScrapedTournamentCourts(
        "http://www.some-site.com/",
        "Stockholm, Sweden",
        LocalDate.now(),
        List.of(
            new ScrapedCourtSchedule(
                "Court Name",
                List.of(
                    new ScrapedMatch(
                        LocalTime.of(11, 30),
                        MatchType.SINGLES,
                        Stage.MAIN_DRAW,
                        ScrapedTeam.singles("some-url", "Name"),
                        ScrapedTeam.singles("another-url", "Name 2"),
                        null,
                        ScrapedTeam.doubles("third-url", "fourth-url", "Name 3", "Name 4")
                    )
                )
            )
        )
    );

    scrapedTournamentCourtsValidator.validate(tournamentCourts);
  }

  @Test(expected = IllegalArgumentException.class)
  public void invalidAlternative1AllNull() {
    ScrapedTournamentCourts tournamentCourts = new ScrapedTournamentCourts(
        "http://www.some-site.com/",
        "Stockholm, Sweden",
        LocalDate.now(),
        List.of(
            new ScrapedCourtSchedule(
                "Court Name",
                List.of(
                    new ScrapedMatch(
                        LocalTime.of(11, 30),
                        MatchType.SINGLES,
                        Stage.MAIN_DRAW,
                        ScrapedTeam.singles("some-url", "Name"),
                        ScrapedTeam.singles("another-url", "Name 2"),
                        ScrapedTeam.singles(null, null),
                        null
                    )
                )
            )
        )
    );

    scrapedTournamentCourtsValidator.validate(tournamentCourts);
  }

  @Test(expected = IllegalArgumentException.class)
  public void invalidAlternative2AllNull() {
    ScrapedTournamentCourts tournamentCourts = new ScrapedTournamentCourts(
        "http://www.some-site.com/",
        "Stockholm, Sweden",
        LocalDate.now(),
        List.of(
            new ScrapedCourtSchedule(
                "Court Name",
                List.of(
                    new ScrapedMatch(
                        LocalTime.of(11, 30),
                        MatchType.SINGLES,
                        Stage.MAIN_DRAW,
                        ScrapedTeam.singles("some-url", "Name"),
                        ScrapedTeam.singles("another-url", "Name 2"),
                        null,
                        ScrapedTeam.singles(null, null)
                    )
                )
            )
        )
    );

    scrapedTournamentCourtsValidator.validate(tournamentCourts);
  }
}
