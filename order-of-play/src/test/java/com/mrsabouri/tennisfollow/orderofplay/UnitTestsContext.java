package com.mrsabouri.tennisfollow.orderofplay;

import com.mrsabouri.tennisfollow.test.groups.BuildTests;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

@Category(BuildTests.class)
@RunWith(MockitoJUnitRunner.class)
public class UnitTestsContext {
}
