package com.mrsabouri.tennisfollow.orderofplay.scraper.service;

import com.mrsabouri.tennisfollow.orderofplay.NightlyTestsContext;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;
import java.time.ZoneId;

import static org.junit.Assert.assertEquals;

public class ZoneIdTests extends NightlyTestsContext {
  @Autowired
  private ZoneIdService zoneIdService;

  @Test
  public void stockholm() {
    ZoneId zoneId = zoneIdService.fromLocation("Stockholm, Sweden", LocalDate.now());
    assertEquals(ZoneId.of("Europe/Stockholm"), zoneId);
  }
}
