package com.mrsabouri.tennisfollow.orderofplay.mock;

import com.mrsabouri.tennisfollow.bracketclient.BracketClient;
import com.mrsabouri.tennisfollow.bracketclient.model.Match;
import com.mrsabouri.tennisfollow.bracketclient.model.MatchByPlayersRequest;
import com.mrsabouri.tennisfollow.bracketclient.model.Tournament;
import com.mrsabouri.tennisfollow.bracketclient.model.enums.BracketType;
import com.mrsabouri.tennisfollow.bracketclient.model.enums.Round;
import com.mrsabouri.tennisfollow.test.interfaces.Cleanable;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

@Component
@Primary
public class BracketMock implements BracketClient, Cleanable {
  public static final String URL1 = UUID.randomUUID().toString();

  private static Map<String, Integer> ID_FOR_URL = Map.ofEntries(
      Map.entry(URL1, 1)
  );

  private Map<MatchByPlayersRequest, Long> matchIdMap;

  public BracketMock() {
    matchIdMap = new HashMap<>();
  }

  @Override
  public void clean() {
    matchIdMap.clear();
  }

  @Override
  public Optional<Tournament> getTournamentByUrl(String tournamentUrl) {
    if (ID_FOR_URL.containsKey(tournamentUrl)) {
      return Optional.of(new Tournament() {
        @Override
        public long getId() {
          return ID_FOR_URL.get(tournamentUrl);
        }
      });
    } else {
      return Optional.empty();
    }
  }

  @Override
  public Optional<Tournament> getTournamentById(long tournamentId) {
    return Optional.empty();
  }

  @Override
  public Optional<Match> getMatchByPlayers(MatchByPlayersRequest matchByPlayersRequest) {
    return getForRequest(matchByPlayersRequest);
  }

  @Override
  public Optional<Match> getNextRoundMatchByPlayers(MatchByPlayersRequest matchByPlayersRequest) {
    return getForRequest(matchByPlayersRequest);
  }

  @Override
  public Optional<Match> getMatchByRoundIndex(long tournamentId, BracketType bracketType, Round round, int roundIndex) {
    return Optional.empty();
  }

  @Override
  public Optional<Match> getMatchById(long matchId) {
    return Optional.empty();
  }

  @Override
  public Round getPreviousRound(Round round) {
    return null;
  }

  public boolean hasValue(long matchId) {
    return matchIdMap.values().contains(matchId);
  }

  private Optional<Match> getForRequest(MatchByPlayersRequest matchByPlayersRequest) {
    if (!matchIdMap.containsKey(matchByPlayersRequest)) {
      matchIdMap.put(matchByPlayersRequest, (long) (Math.random() * 1000));
    }

    Long matchId = matchIdMap.get(matchByPlayersRequest);

    if (matchId == null) {
      return Optional.empty();
    } else {
      return matchMock(matchId);
    }
  }

  private Optional<Match> matchMock(long matchId) {
    return Optional.of(
        new Match() {
          @Override
          public long getId() {
            return matchId;
          }
        }
    );
  }
}
