package com.mrsabouri.tennisfollow.orderofplay.scraper.service;

import com.mrsabouri.tennisfollow.orderofplay.UnitTestsContext;
import com.mrsabouri.tennisfollow.orderofplay.scraper.gateway.ScheduleExtractor;
import com.mrsabouri.tennisfollow.orderofplay.scraper.job.SchedulesJob;
import com.mrsabouri.tennisfollow.orderofplay.scraper.job.impl.SchedulesJobImpl;
import com.mrsabouri.tennisfollow.orderofplay.scraper.model.ScrapedCourtSchedule;
import com.mrsabouri.tennisfollow.orderofplay.scraper.model.ScrapedTournamentCourts;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.util.List;

import static org.mockito.Mockito.*;

public class SchedulesJobUnitTests extends UnitTestsContext {
  @Mock
  private ScheduleExtractor scheduleExtractor;

  @Mock
  private ScrapedTournamentCourtsValidator scrapedTournamentCourtsValidator;

  @Mock
  private ScrapedScheduleToEntityConverter scrapedScheduleToEntityConverter;

  @Mock
  private CourtSchedulePersistor courtSchedulePersistor;

  @Mock
  private TournamentCourtsCleaner tournamentCourtsCleaner;

  private SchedulesJob schedulesJob;

  private List<ScrapedTournamentCourts> tournamentCourtsList;

  @Before
  public void setup() {
    schedulesJob = new SchedulesJobImpl(scheduleExtractor, scrapedTournamentCourtsValidator, scrapedScheduleToEntityConverter, courtSchedulePersistor, tournamentCourtsCleaner);

    tournamentCourtsList = List.of(
        new ScrapedTournamentCourts(null, null, null, List.of(
            new ScrapedCourtSchedule(null, null),
            new ScrapedCourtSchedule(null, null)
            )),
        new ScrapedTournamentCourts(null, null, null, List.of(
            new ScrapedCourtSchedule(null, null),
            new ScrapedCourtSchedule(null, null)
        ))
    );
  }

  @Test
  public void verifyNumberOfCallsWhenNoFailure() {
    when(scheduleExtractor.getTournaments()).thenReturn(tournamentCourtsList);

    schedulesJob.trigger();

    verify(scrapedTournamentCourtsValidator, times(2)).validate(any());
    verify(tournamentCourtsCleaner, times(2)).cleanInvalidCourts(any());
    verify(scrapedScheduleToEntityConverter, times(4)).convert(any(), any());
    verify(courtSchedulePersistor, times(4)).persist(any());

    verifyNoMoreInteractions(scrapedTournamentCourtsValidator);
    verifyNoMoreInteractions(scrapedScheduleToEntityConverter);
    verifyNoMoreInteractions(courtSchedulePersistor);
    verifyNoMoreInteractions(tournamentCourtsCleaner);
  }

  @Test
  public void scheduleExtractorFails() {
    when(scheduleExtractor.getTournaments()).thenThrow(new RuntimeException());

    try {
      schedulesJob.trigger();
    } catch (RuntimeException ignored) {}

    verifyZeroInteractions(scrapedTournamentCourtsValidator);
    verifyZeroInteractions(scrapedScheduleToEntityConverter);
    verifyZeroInteractions(courtSchedulePersistor);
    verifyZeroInteractions(tournamentCourtsCleaner);
  }

  @Test
  public void scrapedTournamentCourtsValidatorFailsOnce() {
    when(scheduleExtractor.getTournaments()).thenReturn(tournamentCourtsList);
    doThrow(new RuntimeException()).doNothing().when(scrapedTournamentCourtsValidator).validate(any());

    schedulesJob.trigger();

    verify(scrapedTournamentCourtsValidator, times(2)).validate(any());
    verify(tournamentCourtsCleaner, times(1)).cleanInvalidCourts(any());
    verify(scrapedScheduleToEntityConverter, times(2)).convert(any(), any());
    verify(courtSchedulePersistor, times(2)).persist(any());

    verifyNoMoreInteractions(scrapedTournamentCourtsValidator);
    verifyNoMoreInteractions(scrapedScheduleToEntityConverter);
    verifyNoMoreInteractions(courtSchedulePersistor);
    verifyNoMoreInteractions(tournamentCourtsCleaner);
  }

  @Test
  public void tournamentCourtsCleanerFailsOnce() {
    when(scheduleExtractor.getTournaments()).thenReturn(tournamentCourtsList);
    doThrow(new RuntimeException()).doNothing().when(tournamentCourtsCleaner).cleanInvalidCourts(any());

    schedulesJob.trigger();

    verify(scrapedTournamentCourtsValidator, times(2)).validate(any());
    verify(tournamentCourtsCleaner, times(2)).cleanInvalidCourts(any());
    verify(scrapedScheduleToEntityConverter, times(2)).convert(any(), any());
    verify(courtSchedulePersistor, times(2)).persist(any());

    verifyNoMoreInteractions(scrapedTournamentCourtsValidator);
    verifyNoMoreInteractions(scrapedScheduleToEntityConverter);
    verifyNoMoreInteractions(courtSchedulePersistor);
    verifyNoMoreInteractions(tournamentCourtsCleaner);
  }

  @Test
  public void scrapedScheduleToEntityConverterFailsOnce() {
    when(scheduleExtractor.getTournaments()).thenReturn(tournamentCourtsList);
    doThrow(new RuntimeException()).doReturn(null).when(scrapedScheduleToEntityConverter).convert(any(), any());

    schedulesJob.trigger();

    verify(scrapedTournamentCourtsValidator, times(2)).validate(any());
    verify(tournamentCourtsCleaner, times(2)).cleanInvalidCourts(any());
    verify(scrapedScheduleToEntityConverter, times(4)).convert(any(), any());
    verify(courtSchedulePersistor, times(3)).persist(any());

    verifyNoMoreInteractions(scrapedTournamentCourtsValidator);
    verifyNoMoreInteractions(scrapedScheduleToEntityConverter);
    verifyNoMoreInteractions(courtSchedulePersistor);
    verifyNoMoreInteractions(tournamentCourtsCleaner);
  }
}
