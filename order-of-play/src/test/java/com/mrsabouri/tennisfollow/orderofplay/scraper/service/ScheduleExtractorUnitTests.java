package com.mrsabouri.tennisfollow.orderofplay.scraper.service;

import com.mrsabouri.lib.jsouputils.DocumentLoader;
import com.mrsabouri.tennisfollow.orderofplay.NightlyUnitTestsContext;
import com.mrsabouri.tennisfollow.orderofplay.scraper.gateway.ScheduleExtractor;
import com.mrsabouri.tennisfollow.orderofplay.scraper.gateway.impl.ScheduleExtractorImpl;
import com.mrsabouri.tennisfollow.orderofplay.scraper.model.ScrapedTournamentCourts;
import com.mrsabouri.tennisfollow.orderofplay.scraper.service.impl.ScrapedTournamentCourtsValidatorImpl;
import org.junit.Before;
import org.junit.Test;
import org.springframework.web.client.RestTemplate;

import java.util.List;

import static org.junit.Assert.assertTrue;

public class ScheduleExtractorUnitTests extends NightlyUnitTestsContext {
  private ScheduleExtractor scheduleExtractor;
  private ScrapedTournamentCourtsValidator scrapedTournamentCourtsValidator;

  @Before
  public void setup() {
    scheduleExtractor = new ScheduleExtractorImpl(new DocumentLoader(new RestTemplate()));
    scrapedTournamentCourtsValidator = new ScrapedTournamentCourtsValidatorImpl();
  }

  @Test
  public void everything() {
    List<ScrapedTournamentCourts> tournaments = scheduleExtractor.getTournaments();
    assertTrue(tournaments.size() > 0);

    for (ScrapedTournamentCourts tournamentCourts : tournaments) {
      scrapedTournamentCourtsValidator.validate(tournamentCourts);
      assertTrue(tournamentCourts.getTournamentUrl().startsWith("https://www.atptour.com/en/tournaments"));
    }
  }
}
