package com.mrsabouri.tennisfollow.orderofplay.service;

import com.mrsabouri.tennisfollow.orderofplay.OrderOfPlayApplicationTestContext;
import com.mrsabouri.tennisfollow.orderofplay.service.persistence.entity.CourtScheduleEntity;
import org.junit.Test;

import java.time.LocalDate;
import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;

public class CourtScheduleRepositoryTests extends OrderOfPlayApplicationTestContext {
  @Test
  public void allDays_noDay() {
    List<LocalDate> allDays = courtScheduleRepository.allDays();
    assertTrue(allDays.isEmpty());
  }

  @Test
  public void allDays_fewDays() {
    courtScheduleRepository.save(CourtScheduleEntity.of(1L, LocalDate.now(), "Some court"));
    courtScheduleRepository.save(CourtScheduleEntity.of(1L, LocalDate.now().plusDays(2), "Some court"));
    courtScheduleRepository.save(CourtScheduleEntity.of(1L, LocalDate.now().minusDays(2), "Some court"));
    courtScheduleRepository.save(CourtScheduleEntity.of(1L, LocalDate.now(), "Another court"));
    courtScheduleRepository.save(CourtScheduleEntity.of(2L, LocalDate.now(), "Another tournament"));

    List<LocalDate> allDays = courtScheduleRepository.allDays();
    assertEquals(3, allDays.size());
    assertEquals(LocalDate.now().plusDays(2), allDays.get(0));
    assertEquals(LocalDate.now(), allDays.get(1));
    assertEquals(LocalDate.now().minusDays(2), allDays.get(2));
  }
}
