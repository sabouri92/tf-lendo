package com.mrsabouri.tennisfollow.orderofplay;

import com.mrsabouri.tennisfollow.test.groups.NightlyTests;
import org.junit.experimental.categories.Category;
import org.springframework.test.context.ActiveProfiles;

@Category(NightlyTests.class)
@ActiveProfiles("no-mock")
public class NightlyTestsContext extends OrderOfPlayApplicationTestContext {
}
