package com.mrsabouri.tennisfollow.orderofplay.mock;

import com.mrsabouri.tennisfollow.orderofplay.scraper.service.ZoneIdService;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.ZoneId;

@Component
@Primary
@Profile("!no-mock")
public class ZoneIdServiceMock implements ZoneIdService {
  private static final ZoneId ZONE_ID = ZoneId.of("UTC");

  @Override
  public ZoneId fromLocation(String location, LocalDate day) {
    return ZONE_ID;
  }

  public ZoneId getZoneId() {
    return ZONE_ID;
  }
}
