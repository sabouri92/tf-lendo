package com.mrsabouri.tennisfollow.orderofplay.scraper.service;

import com.mrsabouri.tennisfollow.bracketclient.BracketClient;
import com.mrsabouri.tennisfollow.bracketclient.model.Match;
import com.mrsabouri.tennisfollow.bracketclient.model.Tournament;
import com.mrsabouri.tennisfollow.orderofplay.UnitTestsContext;
import com.mrsabouri.tennisfollow.orderofplay.enums.MatchType;
import com.mrsabouri.tennisfollow.orderofplay.enums.Stage;
import com.mrsabouri.tennisfollow.orderofplay.scraper.model.ScrapedCourtSchedule;
import com.mrsabouri.tennisfollow.orderofplay.scraper.model.ScrapedMatch;
import com.mrsabouri.tennisfollow.orderofplay.scraper.model.ScrapedTeam;
import com.mrsabouri.tennisfollow.orderofplay.scraper.model.ScrapedTournamentCourts;
import com.mrsabouri.tennisfollow.orderofplay.scraper.persistence.entity.UnknownMatchEntity;
import com.mrsabouri.tennisfollow.orderofplay.scraper.persistence.entity.UnknownTeam;
import com.mrsabouri.tennisfollow.orderofplay.scraper.service.impl.ScrapedScheduleToEntityConverterImpl;
import com.mrsabouri.tennisfollow.orderofplay.service.persistence.entity.CourtScheduleEntity;
import com.mrsabouri.tennisfollow.orderofplay.service.persistence.entity.MatchEntity;
import com.mrsabouri.tennisfollow.orderofplay.service.persistence.repository.CourtScheduleRepository;
import com.mrsabouri.tennisfollow.playerregistry.PlayerRegistryClient;
import com.mrsabouri.tennisfollow.playerregistry.model.Player;
import com.mrsabouri.tennisfollow.playerregistry.model.PlayerByUrlResponse;
import com.mrsabouri.tennisfollow.playerregistry.model.PlayerByUrlStatus;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

public class ScrapedScheduleToEntityConverterTests extends UnitTestsContext {
  private static final String URL = "some-url";
  private static final String LOCATION = "Some Location";
  private static final String COURT = "Some court";
  private static final long TOURNAMENT_ID = 1;
  private static final LocalDate DAY = LocalDate.now();
  private static final LocalTime NOT_BEFORE = LocalTime.of(13, 0);
  private static final ZoneId UTC = ZoneId.of("UTC");
  private static final Instant INSTANT_NOT_BEFORE = DAY.atTime(NOT_BEFORE).atZone(UTC).toInstant();

  private static final String PLAYER_URL1 = "some-url1";
  private static final String PLAYER_URL2 = "some-url2";
  private static final String PLAYER_URL3 = "some-url3";
  private static final String PLAYER_URL4 = "some-url4";

  private static final String PLAYER_NAME1 = "Player Name 1";
  private static final String PLAYER_NAME2 = "Player Name 2";
  private static final String PLAYER_NAME3 = "Player Name 3";
  private static final String PLAYER_NAME4 = "Player Name 4";

  private static final long PLAYER_ID1 = 1;
  private static final long PLAYER_ID2 = 2;
  private static final long PLAYER_ID3 = 3;
  private static final long PLAYER_ID4 = 4;

  private static final long MATCH_ID1 = 1;
  private static final long MATCH_ID2 = 2;

  @Mock
  private BracketClient bracketClient;

  @Mock
  private CourtScheduleRepository courtScheduleRepository;

  @Mock
  private PlayerRegistryClient playerRegistryClient;

  @Mock
  private ZoneIdService zoneIdService;

  private ScrapedScheduleToEntityConverter scrapedScheduleToEntityConverter;

  @Before
  public void setup() {
    scrapedScheduleToEntityConverter = new ScrapedScheduleToEntityConverterImpl(bracketClient, courtScheduleRepository, playerRegistryClient, zoneIdService);
  }

  @Test
  public void happyPath() {
    ScrapedMatch match1 = new ScrapedMatch(
        NOT_BEFORE,
        MatchType.DOUBLES,
        Stage.QUALIFIER,
        ScrapedTeam.doubles(PLAYER_URL1, PLAYER_URL2, PLAYER_NAME1, PLAYER_NAME2),
        ScrapedTeam.doubles(PLAYER_URL3, PLAYER_URL4, PLAYER_NAME3, PLAYER_NAME4)
    );

    ScrapedMatch match2 = new ScrapedMatch(
        null,
        MatchType.SINGLES,
        Stage.QUALIFIER,
        ScrapedTeam.singles(PLAYER_URL1, PLAYER_NAME1),
        ScrapedTeam.singles(PLAYER_URL2, PLAYER_NAME2),
        ScrapedTeam.singles(PLAYER_URL3, PLAYER_NAME3),
        ScrapedTeam.singles(PLAYER_URL4, PLAYER_NAME4)
    );

    ScrapedCourtSchedule courtSchedule = new ScrapedCourtSchedule(COURT, List.of(match1, match2));

    ScrapedTournamentCourts tournamentCourts = new ScrapedTournamentCourts(
        URL, LOCATION, DAY, List.of(courtSchedule)
    );

    when(bracketClient.getTournamentByUrl(eq(URL))).thenReturn(Optional.of(tournamentMock(TOURNAMENT_ID)));
    when(bracketClient.getMatchByPlayers(any())).thenReturn(matchMock(MATCH_ID1));
    when(bracketClient.getNextRoundMatchByPlayers(any())).thenReturn(matchMock(MATCH_ID2));
    when(playerRegistryClient.getPlayerByUrl(eq(PLAYER_URL1))).thenReturn(playerRegistryResponse(PLAYER_ID1));
    when(playerRegistryClient.getPlayerByUrl(eq(PLAYER_URL2))).thenReturn(playerRegistryResponse(PLAYER_ID2));
    when(playerRegistryClient.getPlayerByUrl(eq(PLAYER_URL3))).thenReturn(playerRegistryResponse(PLAYER_ID3));
    when(playerRegistryClient.getPlayerByUrl(eq(PLAYER_URL4))).thenReturn(playerRegistryResponse(PLAYER_ID4));
    when(zoneIdService.fromLocation(eq(LOCATION), any())).thenReturn(UTC);

    CourtScheduleEntity courtScheduleEntity = scrapedScheduleToEntityConverter.convert(tournamentCourts, courtSchedule);

    assertEquals(2, courtScheduleEntity.getMatches().size());
    assertNotNull(courtScheduleEntity.getMatches().get(0));
    assertNotNull(courtScheduleEntity.getMatches().get(1));
    assertNull(courtScheduleEntity.getMatches().get(0).getUnknownMatch());
    assertNull(courtScheduleEntity.getMatches().get(1).getUnknownMatch());

    MatchEntity matchEntity1 = courtScheduleEntity.getMatches().get(0);
    assertEquals(courtScheduleEntity, matchEntity1.getCourtSchedule());
    assertEquals(0, matchEntity1.getIndex());
    assertEquals(INSTANT_NOT_BEFORE, matchEntity1.getNotBefore());
    assertEquals(MATCH_ID1, (long) matchEntity1.getMatchId());
    assertNull(matchEntity1.getUnknownMatch());

    MatchEntity matchEntity2 = courtScheduleEntity.getMatches().get(1);
    assertEquals(courtScheduleEntity, matchEntity2.getCourtSchedule());
    assertEquals(1, matchEntity2.getIndex());
    assertNull(matchEntity2.getNotBefore());
    assertEquals(MATCH_ID2, (long) matchEntity2.getMatchId());
    assertNull(matchEntity2.getUnknownMatch());
  }

  @Test(expected = RuntimeException.class)
  public void tournamentDoesNotExist() {
    ScrapedTournamentCourts tournamentCourts = new ScrapedTournamentCourts(
        URL, LOCATION, DAY, List.of()
    );

    ScrapedCourtSchedule courtSchedule = new ScrapedCourtSchedule("Some Court", List.of());

    when(bracketClient.getTournamentByUrl(any())).thenReturn(Optional.empty());

    scrapedScheduleToEntityConverter.convert(tournamentCourts, courtSchedule);
  }

  @Test
  public void singlesPlayersDoesNotExist() {
    ScrapedMatch match = new ScrapedMatch(
        NOT_BEFORE,
        MatchType.SINGLES,
        Stage.QUALIFIER,
        ScrapedTeam.singles(PLAYER_URL1, PLAYER_NAME1),
        ScrapedTeam.singles(PLAYER_URL2, PLAYER_NAME2),
        ScrapedTeam.singles(PLAYER_URL3, PLAYER_NAME3),
        ScrapedTeam.singles(PLAYER_URL4, PLAYER_NAME4)
    );

    ScrapedCourtSchedule courtSchedule = new ScrapedCourtSchedule(COURT, List.of(match));

    ScrapedTournamentCourts tournamentCourts = new ScrapedTournamentCourts(
        URL, LOCATION, DAY, List.of(courtSchedule)
    );

    when(bracketClient.getTournamentByUrl(eq(URL))).thenReturn(Optional.of(tournamentMock(TOURNAMENT_ID)));
    when(playerRegistryClient.getPlayerByUrl(any())).thenReturn(playerRegistryResponse(PlayerByUrlStatus.TEMP_FAILURE));
    when(zoneIdService.fromLocation(eq(LOCATION), any())).thenReturn(UTC);

    CourtScheduleEntity courtScheduleEntity = scrapedScheduleToEntityConverter.convert(tournamentCourts, courtSchedule);

    assertEquals(1, courtScheduleEntity.getMatches().size());
    assertNotNull(courtScheduleEntity.getMatches().get(0));
    assertNotNull(courtScheduleEntity.getMatches().get(0).getUnknownMatch());
    assertNull(courtScheduleEntity.getMatches().get(0).getMatchId());

    UnknownMatchEntity unknownMatch = courtScheduleEntity.getMatches().get(0).getUnknownMatch();
    assertNotNull(unknownMatch.getTeam1());
    assertSinglesTeam(unknownMatch.getTeam1(), PLAYER_URL1, PLAYER_NAME1);
    assertSinglesTeam(unknownMatch.getTeam2(), PLAYER_URL2, PLAYER_NAME2);
    assertSinglesTeam(unknownMatch.getAlternativeTeam1(), PLAYER_URL3, PLAYER_NAME3);
    assertSinglesTeam(unknownMatch.getAlternativeTeam2(), PLAYER_URL4, PLAYER_NAME4);
  }

  @Test
  public void onlyOnePlayerDoesNotExist() {
    ScrapedMatch match = new ScrapedMatch(
        NOT_BEFORE,
        MatchType.SINGLES,
        Stage.QUALIFIER,
        ScrapedTeam.singles(PLAYER_URL1, PLAYER_NAME1),
        ScrapedTeam.singles(PLAYER_URL2, PLAYER_NAME2)
    );

    ScrapedCourtSchedule courtSchedule = new ScrapedCourtSchedule(COURT, List.of(match));

    ScrapedTournamentCourts tournamentCourts = new ScrapedTournamentCourts(
        URL, LOCATION, DAY, List.of(courtSchedule)
    );

    when(bracketClient.getTournamentByUrl(eq(URL))).thenReturn(Optional.of(tournamentMock(TOURNAMENT_ID)));
    when(playerRegistryClient.getPlayerByUrl(eq(PLAYER_URL1))).thenReturn(playerRegistryResponse(PLAYER_ID1));
    when(playerRegistryClient.getPlayerByUrl(eq(PLAYER_URL2))).thenReturn(playerRegistryResponse(PlayerByUrlStatus.TEMP_FAILURE));
    when(zoneIdService.fromLocation(eq(LOCATION), any())).thenReturn(UTC);

    CourtScheduleEntity courtScheduleEntity = scrapedScheduleToEntityConverter.convert(tournamentCourts, courtSchedule);

    assertEquals(1, courtScheduleEntity.getMatches().size());
    assertNotNull(courtScheduleEntity.getMatches().get(0));
    assertNotNull(courtScheduleEntity.getMatches().get(0).getUnknownMatch());
    assertNull(courtScheduleEntity.getMatches().get(0).getMatchId());

    UnknownMatchEntity unknownMatch = courtScheduleEntity.getMatches().get(0).getUnknownMatch();
    assertNotNull(unknownMatch.getTeam1());
    assertSinglesTeam(unknownMatch.getTeam1(), PLAYER_URL1, PLAYER_NAME1);
    assertSinglesTeam(unknownMatch.getTeam2(), PLAYER_URL2, PLAYER_NAME2);
    assertSinglesTeam(unknownMatch.getAlternativeTeam1(), null, null);
    assertSinglesTeam(unknownMatch.getAlternativeTeam2(), null, null);
  }

  @Test
  public void matchDoesNotExist() {
    ScrapedMatch match = new ScrapedMatch(
        NOT_BEFORE,
        MatchType.SINGLES,
        Stage.QUALIFIER,
        ScrapedTeam.singles(PLAYER_URL1, PLAYER_NAME1),
        ScrapedTeam.singles(PLAYER_URL2, PLAYER_NAME2)
    );

    ScrapedCourtSchedule courtSchedule = new ScrapedCourtSchedule(COURT, List.of(match));

    ScrapedTournamentCourts tournamentCourts = new ScrapedTournamentCourts(
        URL, LOCATION, DAY, List.of(courtSchedule)
    );

    when(bracketClient.getTournamentByUrl(eq(URL))).thenReturn(Optional.of(tournamentMock(TOURNAMENT_ID)));
    when(bracketClient.getMatchByPlayers(any())).thenReturn(Optional.empty());
    when(playerRegistryClient.getPlayerByUrl(eq(PLAYER_URL1))).thenReturn(playerRegistryResponse(PLAYER_ID1));
    when(playerRegistryClient.getPlayerByUrl(eq(PLAYER_URL2))).thenReturn(playerRegistryResponse(PLAYER_ID2));
    when(zoneIdService.fromLocation(eq(LOCATION), any())).thenReturn(UTC);

    CourtScheduleEntity courtScheduleEntity = scrapedScheduleToEntityConverter.convert(tournamentCourts, courtSchedule);

    assertEquals(1, courtScheduleEntity.getMatches().size());
    assertNotNull(courtScheduleEntity.getMatches().get(0));
    assertNotNull(courtScheduleEntity.getMatches().get(0).getUnknownMatch());
    assertNull(courtScheduleEntity.getMatches().get(0).getMatchId());

    UnknownMatchEntity unknownMatch = courtScheduleEntity.getMatches().get(0).getUnknownMatch();
    assertNotNull(unknownMatch.getTeam1());
    assertSinglesTeam(unknownMatch.getTeam1(), PLAYER_URL1, PLAYER_NAME1);
    assertSinglesTeam(unknownMatch.getTeam2(), PLAYER_URL2, PLAYER_NAME2);
    assertSinglesTeam(unknownMatch.getAlternativeTeam1(), null, null);
    assertSinglesTeam(unknownMatch.getAlternativeTeam2(), null, null);
  }

  @Test
  public void testPicksExistingEntity() {
    ScrapedMatch match = new ScrapedMatch(
        NOT_BEFORE,
        MatchType.SINGLES,
        Stage.QUALIFIER,
        ScrapedTeam.singles(PLAYER_URL1, PLAYER_NAME1),
        ScrapedTeam.singles(PLAYER_URL2, PLAYER_NAME2)
    );

    ScrapedCourtSchedule courtSchedule = new ScrapedCourtSchedule(COURT, List.of(match));

    ScrapedTournamentCourts tournamentCourts = new ScrapedTournamentCourts(
        URL, LOCATION, DAY, List.of(courtSchedule)
    );

    CourtScheduleEntity existingCourtScheduleEntity = new CourtScheduleEntity();
    existingCourtScheduleEntity.setId(123L);

    when(bracketClient.getTournamentByUrl(eq(URL))).thenReturn(Optional.of(tournamentMock(TOURNAMENT_ID)));
    when(bracketClient.getMatchByPlayers(any())).thenReturn(matchMock(MATCH_ID1));
    when(playerRegistryClient.getPlayerByUrl(eq(PLAYER_URL1))).thenReturn(playerRegistryResponse(PLAYER_ID1));
    when(playerRegistryClient.getPlayerByUrl(eq(PLAYER_URL2))).thenReturn(playerRegistryResponse(PLAYER_ID2));
    when(zoneIdService.fromLocation(eq(LOCATION), any())).thenReturn(UTC);
    when(courtScheduleRepository.findByTournamentIdAndDayAndCourtName(eq(TOURNAMENT_ID), eq(DAY), eq(COURT))).thenReturn(Optional.of(existingCourtScheduleEntity));

    CourtScheduleEntity courtScheduleEntity = scrapedScheduleToEntityConverter.convert(tournamentCourts, courtSchedule);

    assertEquals(123L, (long) courtScheduleEntity.getId());
    assertSame(existingCourtScheduleEntity, courtScheduleEntity);
  }

  private void assertSinglesTeam(UnknownTeam team, String url, String name) {
    assertEquals(url, team.getUrl1());
    assertEquals(name, team.getName1());
    assertNull(team.getUrl2());
    assertNull(team.getName2());
  }

  private Tournament tournamentMock(long tournamentId) {
    return new Tournament() {
      @Override
      public long getId() {
        return tournamentId;
      }
    };
  }

  private Optional<Match> matchMock(long matchId) {
    return Optional.of(new Match() {
      @Override
      public long getId() {
        return matchId;
      }
    });
  }

  private PlayerByUrlResponse playerRegistryResponse(PlayerByUrlStatus playerByUrlStatus) {
    return new PlayerByUrlResponse() {
      @Override
      public PlayerByUrlStatus getStatus() {
        return playerByUrlStatus;
      }
    };
  }

  private PlayerByUrlResponse playerRegistryResponse(long playerId) {
    return new PlayerByUrlResponse() {
      @Override
      public PlayerByUrlStatus getStatus() {
        return PlayerByUrlStatus.EXISTS;
      }

      @Override
      public Player getPlayer() {
        return new Player() {
          @Override
          public long getId() {
            return playerId;
          }
        };
      }
    };
  }
}
