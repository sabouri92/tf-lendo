package com.mrsabouri.tennisfollow.orderofplay.scraper;

import com.mrsabouri.tennisfollow.orderofplay.OrderOfPlayApplicationTestContext;
import com.mrsabouri.tennisfollow.orderofplay.mock.BracketMock;
import com.mrsabouri.tennisfollow.orderofplay.mock.ScheduleExtractorMock;
import com.mrsabouri.tennisfollow.orderofplay.mock.ZoneIdServiceMock;
import com.mrsabouri.tennisfollow.orderofplay.scraper.model.ScrapedCourtSchedule;
import com.mrsabouri.tennisfollow.orderofplay.scraper.model.ScrapedMatch;
import com.mrsabouri.tennisfollow.orderofplay.scraper.model.ScrapedTeam;
import com.mrsabouri.tennisfollow.orderofplay.scraper.model.ScrapedTournamentCourts;
import com.mrsabouri.tennisfollow.orderofplay.scraper.persistence.entity.UnknownMatchEntity;
import com.mrsabouri.tennisfollow.orderofplay.scraper.persistence.entity.UnknownTeam;
import com.mrsabouri.tennisfollow.orderofplay.service.persistence.entity.CourtScheduleEntity;
import com.mrsabouri.tennisfollow.orderofplay.service.persistence.entity.MatchEntity;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Instant;
import java.time.LocalDate;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.awaitility.Awaitility.await;
import static org.junit.Assert.*;

public class SchedulesJobTests extends OrderOfPlayApplicationTestContext {
  @Autowired
  private BracketMock bracketMock;

  @Autowired
  private ScheduleExtractorMock scheduleExtractorMock;

  @Autowired
  private ZoneIdServiceMock zoneIdServiceMock;

  @Test
  public void happyPath() {
    triggerScheduleJob();

    await().atMost(3, TimeUnit.SECONDS).until(() -> courtScheduleRepository.count() == 1);

    List<CourtScheduleEntity> all = courtScheduleRepository.findAll();
    List<ScrapedTournamentCourts> returnValue = scheduleExtractorMock.getToReturn();

    assertEquals(1, all.size());
    assertContent(all.get(0), returnValue.get(0));
  }

  @Test
  public void matchesAdded() {
    // Trigger with 2 matches
    scheduleExtractorMock.setToReturn(2, false);
    triggerScheduleJob();

    await().atMost(3, TimeUnit.SECONDS).until(() -> courtScheduleRepository.count() == 1);

    // Trigger with 3 matches
    scheduleExtractorMock.setToReturn(3, false);
    triggerScheduleJob();

    await().atMost(3, TimeUnit.SECONDS).until(() -> courtScheduleRepository.findAll().get(0).getMatches().size() == 3);

    // Verify correctness
    List<CourtScheduleEntity> all = courtScheduleRepository.findAll();
    List<ScrapedTournamentCourts> returnValue = scheduleExtractorMock.getToReturn();

    assertEquals(1, all.size());
    assertContent(all.get(0), returnValue.get(0));
  }

  @Test
  public void matchesRemoved() {
    // Trigger with 3 matches
    scheduleExtractorMock.setToReturn(3, false);
    triggerScheduleJob();

    await().atMost(3, TimeUnit.SECONDS).until(() -> courtScheduleRepository.count() == 1);

    // Trigger with 2 matches
    scheduleExtractorMock.setToReturn(2, false);
    triggerScheduleJob();

    await().atMost(3, TimeUnit.SECONDS).until(() -> courtScheduleRepository.findAll().get(0).getMatches().size() == 2);

    // Verify correctness
    List<CourtScheduleEntity> all = courtScheduleRepository.findAll();
    List<ScrapedTournamentCourts> returnValue = scheduleExtractorMock.getToReturn();

    assertEquals(1, all.size());
    assertContent(all.get(0), returnValue.get(0));
  }

  @Test
  public void matchesChanged() throws InterruptedException {
    // Trigger
    scheduleExtractorMock.setToReturn(3, false);
    triggerScheduleJob();

    await().atMost(3, TimeUnit.SECONDS).until(() -> courtScheduleRepository.count() == 1);

    // Trigger with changed matches
    scheduleExtractorMock.setToReturn(3, true);
    triggerScheduleJob();

    Thread.sleep(3000); // No await I can think of. :(

    // Verify correctness
    List<CourtScheduleEntity> all = courtScheduleRepository.findAll();
    List<ScrapedTournamentCourts> returnValue = scheduleExtractorMock.getToReturn();

    assertEquals(1, all.size());
    assertContent(all.get(0), returnValue.get(0));
  }

  @SuppressWarnings("OptionalGetWithoutIsPresent")
  private void assertContent(CourtScheduleEntity courtScheduleEntity, ScrapedTournamentCourts scrapedTournamentCourts) {
    ScrapedCourtSchedule scrapedCourtSchedule = scrapedTournamentCourts.getCourtSchedules().get(0);

    assertEquals(bracketMock.getTournamentByUrl(scrapedTournamentCourts.getTournamentUrl()).get().getId(),
        courtScheduleEntity.getTournamentId());
    assertEquals(scrapedTournamentCourts.getDay(), courtScheduleEntity.getDay());
    assertEquals(scrapedCourtSchedule.getCourtName(), courtScheduleEntity.getCourtName());

    List<ScrapedMatch> scrapedMatches = scrapedCourtSchedule.getScrapedMatches();
    List<MatchEntity> matchEntities = courtScheduleEntity.getMatches();

    assertEquals(scrapedMatches.size(), matchEntities.size());

    for (int i = 0; i < scrapedMatches.size(); i++) {
      ScrapedMatch scrapedMatch = scrapedMatches.get(i);
      MatchEntity matchEntity = matchEntities.get(i);
      assertEquals(i, matchEntity.getIndex());
      assertContent(scrapedMatch, matchEntity, scrapedTournamentCourts.getDay());
    }
  }

  private void assertContent(ScrapedMatch scrapedMatch, MatchEntity matchEntity, LocalDate day) {
    if (scrapedMatch.getNotBefore() == null) {
      assertNull(matchEntity.getNotBefore());
    } else {
      Instant expected = day.atTime(scrapedMatch.getNotBefore()).atZone(zoneIdServiceMock.getZoneId()).toInstant();
      assertEquals(expected, matchEntity.getNotBefore());
    }

    Long matchId = matchEntity.getMatchId();

    if (scrapedMatch.getTeam1().getUrl1() == null || scrapedMatch.getTeam2().getUrl1() == null) {
      assertNull(matchId);
    }

    if (matchId != null) {
      assertTrue(bracketMock.hasValue(matchId));
      assertNull(matchEntity.getUnknownMatch());
      return;
    }

    UnknownMatchEntity unknownMatchEntity = matchEntity.getUnknownMatch();
    assertEquals(matchEntity, unknownMatchEntity.getMatch());
    assertEquals(scrapedMatch.getMatchType(), unknownMatchEntity.getMatchType());
    assertContent(scrapedMatch.getTeam1(), unknownMatchEntity.getTeam1());
  }

  private void assertContent(ScrapedTeam scrapedTeam, UnknownTeam unknownTeam) {
    assertEquals(scrapedTeam.getUrl1(), unknownTeam.getUrl1());
    assertEquals(scrapedTeam.getUrl2(), unknownTeam.getUrl2());
    assertEquals(scrapedTeam.getName1(), unknownTeam.getName1());
    assertEquals(scrapedTeam.getName2(), unknownTeam.getName2());
  }
}
