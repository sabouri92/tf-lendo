package com.mrsabouri.tennisfollow.orderofplay.scraper.service;

import com.mrsabouri.tennisfollow.orderofplay.OrderOfPlayApplicationTestContext;
import com.mrsabouri.tennisfollow.orderofplay.mock.BracketMock;
import com.mrsabouri.tennisfollow.orderofplay.scraper.model.ScrapedCourtSchedule;
import com.mrsabouri.tennisfollow.orderofplay.scraper.model.ScrapedTournamentCourts;
import com.mrsabouri.tennisfollow.orderofplay.service.persistence.entity.CourtScheduleEntity;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TournamentCourtsCleanerTests extends OrderOfPlayApplicationTestContext {
  private static final String VALID_NAME = "Valid Name";
  private static final String INVALID_NAME = "Invalid Name";
  private static final String SOME_OTHER_NAME = "Some Other Name";

  @Autowired
  private TournamentCourtsCleaner tournamentCourtsCleaner;

  @Test
  public void deletesNonExistentCourts() {
    String tournamentUrl = BracketMock.URL1;
    //noinspection OptionalGetWithoutIsPresent
    long tournamentId = new BracketMock().getTournamentByUrl(tournamentUrl).get().getId();
    String tournamentLocation = "City, Country";
    LocalDate date = LocalDate.now();

    CourtScheduleEntity courtScheduleEntity1 = CourtScheduleEntity.of(tournamentId, date, VALID_NAME);
    CourtScheduleEntity courtScheduleEntity2 = CourtScheduleEntity.of(tournamentId, date, INVALID_NAME);
    courtScheduleRepository.save(courtScheduleEntity1);
    courtScheduleRepository.save(courtScheduleEntity2);

    List<ScrapedTournamentCourts> tournamentCourtsList = List.of(
        new ScrapedTournamentCourts(
            tournamentUrl,
            tournamentLocation,
            date,
            List.of(
                new ScrapedCourtSchedule(VALID_NAME, List.of()),
                new ScrapedCourtSchedule(SOME_OTHER_NAME, List.of())
            )
        )
    );

    assertEquals(2, courtScheduleRepository.count());

    tournamentCourtsList.forEach(tournamentCourtsCleaner::cleanInvalidCourts);

    assertEquals(1, courtScheduleRepository.count());
    assertTrue(courtScheduleRepository.findByTournamentIdAndDayAndCourtName(tournamentId, date, VALID_NAME).isPresent());
    assertTrue(courtScheduleRepository.findByTournamentIdAndDayAndCourtName(tournamentId, date, INVALID_NAME).isEmpty());
  }
}
