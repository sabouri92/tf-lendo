package com.mrsabouri.tennisfollow.playerregistry;

import com.mrsabouri.tennisfollow.playerregistry.model.Player;
import com.mrsabouri.tennisfollow.playerregistry.model.PlayerByUrlResponse;
import com.mrsabouri.tennisfollow.playerregistry.model.PlayerByUrlStatus;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.Mockito.*;

@Import(TestConfiguration.class)
@RunWith(SpringRunner.class)
public class PlayerRegistryClientTest {
  @MockBean
  private RestTemplate restTemplate;

  @Autowired
  private PlayerRegistryClient playerRegistryClient;

  @Test
  public void cachesByUrlIfExists() {
    PlayerByUrlResponse response = playerResponse(PlayerByUrlStatus.EXISTS);

    when(restTemplate.getForObject(any(), any(), anyMap())).thenReturn(response);

    playerRegistryClient.getPlayerByUrl("some-url");
    playerRegistryClient.getPlayerByUrl("some-url");

    verify(restTemplate, times(1)).getForObject(any(), any(), anyMap());
    verifyNoMoreInteractions(restTemplate);
  }

  @Test
  public void noCacheByUrlIfTempFailure() {
    PlayerByUrlResponse response = playerResponse(PlayerByUrlStatus.TEMP_FAILURE);

    when(restTemplate.getForObject(any(), any(), anyMap())).thenReturn(response);

    playerRegistryClient.getPlayerByUrl("some-url-2");
    playerRegistryClient.getPlayerByUrl("some-url-2");

    verify(restTemplate, times(2)).getForObject(any(), any(), anyMap());
  }

  @Test
  public void noCacheByUrlIfPermFailure() {
    PlayerByUrlResponse response = playerResponse(PlayerByUrlStatus.PERM_FAILURE);

    when(restTemplate.getForObject(any(), any(), anyMap())).thenReturn(response);

    playerRegistryClient.getPlayerByUrl("some-url-3");
    playerRegistryClient.getPlayerByUrl("some-url-3");

    verify(restTemplate, times(2)).getForObject(any(), any(), anyMap());
  }

  @Test
  public void cachesByIdIfOK() {
    Player player = mock(Player.class);

    when(restTemplate.getForObject(any(), any(), anyMap())).thenReturn(player);

    playerRegistryClient.playerById(1L);
    playerRegistryClient.playerById(1L);

    verify(restTemplate, times(1)).getForObject(any(), any(), anyMap());
  }

  @Test
  public void noCacheByIdIf404() {
    HttpClientErrorException httpClientErrorException = new HttpClientErrorException(HttpStatus.NOT_FOUND);
    when(restTemplate.getForObject(any(), any(), anyMap())).thenThrow(httpClientErrorException);

    playerRegistryClient.playerById(2L);
    playerRegistryClient.playerById(2L);

    verify(restTemplate, times(2)).getForObject(any(), any(), anyMap());
  }

  @Test
  public void noCacheByIdIfError() {
    when(restTemplate.getForObject(any(), any(), anyMap())).thenThrow(new RuntimeException());

    try {
      playerRegistryClient.playerById(3L);
    } catch (RuntimeException ignored) {}

    try {
      playerRegistryClient.playerById(3L);
    } catch (RuntimeException ignored) {}

    verify(restTemplate, times(2)).getForObject(any(), any(), anyMap());
  }

  private PlayerByUrlResponse playerResponse(PlayerByUrlStatus status) {
    return new PlayerByUrlResponse() {
      @Override
      public PlayerByUrlStatus getStatus() {
        return status;
      }
    };
  }
}
