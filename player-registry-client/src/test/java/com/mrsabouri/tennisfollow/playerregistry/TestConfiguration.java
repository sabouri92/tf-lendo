package com.mrsabouri.tennisfollow.playerregistry;

import com.mrsabouri.tennisfollow.playerregistry.impl.PlayerRegistryClientImpl;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
@EnableCaching
public class TestConfiguration {
  @Bean
  public CacheManager cacheManager() {
    return new ConcurrentMapCacheManager();
  }

  @Bean
  public PlayerRegistryClient playerRegistryClient(RestTemplate restTemplate) {
    return new PlayerRegistryClientImpl(restTemplate);
  }
}
