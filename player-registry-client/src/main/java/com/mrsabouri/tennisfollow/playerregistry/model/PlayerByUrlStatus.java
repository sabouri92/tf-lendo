package com.mrsabouri.tennisfollow.playerregistry.model;

public enum PlayerByUrlStatus {
  EXISTS,
  TEMP_FAILURE,
  PERM_FAILURE
}
