package com.mrsabouri.tennisfollow.playerregistry.impl;

import com.mrsabouri.tennisfollow.playerregistry.PlayerRegistryClient;
import com.mrsabouri.tennisfollow.playerregistry.model.Player;
import com.mrsabouri.tennisfollow.playerregistry.model.PlayerByUrlResponse;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.Map;
import java.util.Optional;

public class PlayerRegistryClientImpl implements PlayerRegistryClient {
  private static final String BASE_URL = "http://player-registry/";

  private RestTemplate restTemplate;
  private String baseUrl;

  public PlayerRegistryClientImpl(RestTemplate restTemplate) {
    this(restTemplate, BASE_URL);
  }

  public PlayerRegistryClientImpl(RestTemplate restTemplate, String baseUrl) {
    this.restTemplate = restTemplate;
    this.baseUrl = baseUrl;
  }

  @Override
  @Cacheable(value = "player-registry/player/by-url", unless = "#result == null || #result.status == null || #result.status.name() != 'EXISTS'")
  public PlayerByUrlResponse getPlayerByUrl(String playerUrl) {
    String apiUrl = baseUrl + "/player/by-url?url={url}";
    return restTemplate.getForObject(apiUrl, PlayerByUrlResponse.class, Map.of("url", playerUrl));
  }

  @Override
  @Cacheable(value = "player-registry/player/by-id", unless = "#result == null")
  public Optional<Player> playerById(long playerId) {
    String apiUrl = baseUrl + "/player/by-id/{playerId}";

    try {
      Player responsePlayer = restTemplate.getForObject(apiUrl, Player.class, Map.of("playerId", playerId));
      if (responsePlayer == null) {
        throw new RuntimeException("Unexpected response from player-registry");
      }
      return Optional.of(responsePlayer);

    } catch (HttpClientErrorException hceex) {
      if (hceex.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
        return Optional.empty();
      }
      throw hceex;
    }
  }
}
