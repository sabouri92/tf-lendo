package com.mrsabouri.tennisfollow.playerregistry.config;

import com.mrsabouri.tennisfollow.playerregistry.PlayerRegistryClient;
import com.mrsabouri.tennisfollow.playerregistry.impl.PlayerRegistryClientImpl;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
@ConditionalOnBean(RestTemplate.class)
public class PlayerRegistryClientAutoConfiguration {
  @Bean
  public PlayerRegistryClient playerRegistryClient(RestTemplate restTemplate) {
    return new PlayerRegistryClientImpl(restTemplate);
  }
}
