package com.mrsabouri.tennisfollow.playerregistry.model;

import java.time.LocalDate;

public class Player {
  private long id;
  private String firstName;
  private String lastName;
  private LocalDate birthday;
  private String countryCode;

  public long getId() {
    return id;
  }

  public String getFirstName() {
    return firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public LocalDate getBirthday() {
    return birthday;
  }

  public String getCountryCode() {
    return countryCode;
  }
}
