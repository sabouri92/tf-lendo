package com.mrsabouri.tennisfollow.playerregistry.model;

public class PlayerByUrlResponse {
  private PlayerByUrlStatus status;
  private Player player;

  public PlayerByUrlStatus getStatus() {
    return status;
  }

  public Player getPlayer() {
    return player;
  }
}
