package com.mrsabouri.tennisfollow.playerregistry;

import com.mrsabouri.tennisfollow.playerregistry.model.Player;
import com.mrsabouri.tennisfollow.playerregistry.model.PlayerByUrlResponse;

import java.util.Optional;

public interface PlayerRegistryClient {
  PlayerByUrlResponse getPlayerByUrl(String url);

  Optional<Player> playerById(long playerId);
}
