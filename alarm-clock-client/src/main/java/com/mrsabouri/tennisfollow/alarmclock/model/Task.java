package com.mrsabouri.tennisfollow.alarmclock.model;

import java.time.Duration;
import java.time.Instant;

public class Task {
  private String name;
  private Integer version;
  private String url;
  private Instant firstInvocation;
  private Duration frequency;

  public Task(String name, Integer version, String url, Instant firstInvocation, Duration frequency) {
    this.name = name;
    this.version = version;
    this.url = url;
    this.firstInvocation = firstInvocation;
    this.frequency = frequency;
  }

  public String getName() {
    return name;
  }

  public Integer getVersion() {
    return version;
  }

  public String getUrl() {
    return url;
  }

  public Instant getFirstInvocation() {
    return firstInvocation;
  }

  public Duration getFrequency() {
    return frequency;
  }

  @Override
  public String toString() {
    return "Task{" +
        "name='" + name + '\'' +
        ", version=" + version +
        ", url='" + url + '\'' +
        ", firstInvocation=" + firstInvocation +
        ", frequency=" + frequency +
        '}';
  }
}
