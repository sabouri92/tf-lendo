package com.mrsabouri.tennisfollow.alarmclock.impl;

import com.mrsabouri.tennisfollow.alarmclock.AlarmClockClient;
import com.mrsabouri.tennisfollow.alarmclock.model.Task;
import org.springframework.web.client.RestTemplate;

public class AlarmClockClientImpl implements AlarmClockClient {
  private static final String BASE_URL = "http://alarm-clock/";

  private RestTemplate restTemplate;
  private String baseUrl;

  public AlarmClockClientImpl(RestTemplate restTemplate) {
    this(restTemplate, BASE_URL);
  }

  public AlarmClockClientImpl(RestTemplate restTemplate, String baseUrl) {
    this.restTemplate = restTemplate;
    this.baseUrl = baseUrl;
  }

  @Override
  public void registerTask(Task task) {
    String url = baseUrl + "/task";
    restTemplate.postForLocation(url, task);
  }
}
