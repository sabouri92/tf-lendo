package com.mrsabouri.tennisfollow.alarmclock.config;

import com.mrsabouri.tennisfollow.alarmclock.AlarmClockClient;
import com.mrsabouri.tennisfollow.alarmclock.impl.AlarmClockClientImpl;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
@ConditionalOnBean(RestTemplate.class)
public class AlarmClockClientAutoConfiguration {
  @Bean
  public AlarmClockClient alarmClockClient(RestTemplate restTemplate) {
    return new AlarmClockClientImpl(restTemplate);
  }
}
