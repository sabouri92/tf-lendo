package com.mrsabouri.tennisfollow.alarmclock;

import com.mrsabouri.tennisfollow.alarmclock.model.Task;

public interface AlarmClockClient {
  void registerTask(Task task);
}
