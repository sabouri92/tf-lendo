package com.mrsabouri.tennisfollow.orderofplayclient.model;

import java.time.LocalDate;

public class ScheduleDay {
  private LocalDate day;

  public LocalDate getDay() {
    return day;
  }
}
