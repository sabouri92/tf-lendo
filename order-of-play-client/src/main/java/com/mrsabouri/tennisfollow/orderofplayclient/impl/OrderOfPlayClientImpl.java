package com.mrsabouri.tennisfollow.orderofplayclient.impl;

import com.mrsabouri.tennisfollow.orderofplayclient.OrderOfPlayClient;
import com.mrsabouri.tennisfollow.orderofplayclient.model.ExpandedSchedule;
import com.mrsabouri.tennisfollow.orderofplayclient.model.ScheduleDay;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

@Component
public class OrderOfPlayClientImpl implements OrderOfPlayClient {
  private static final String BASE_URL = "http://order-of-play/";

  private RestTemplate restTemplate;
  private String baseUrl;

  public OrderOfPlayClientImpl(RestTemplate restTemplate) {
    this(restTemplate, BASE_URL);
  }

  public OrderOfPlayClientImpl(RestTemplate restTemplate, String baseUrl) {
    this.restTemplate = restTemplate;
    this.baseUrl = baseUrl;
  }

  @Override
  public List<ScheduleDay> scheduleDays() {
    String apiUrl = baseUrl + "/schedule/days";
    ResponseEntity<List<ScheduleDay>> scheduleDays = restTemplate.exchange(
        apiUrl,
        HttpMethod.GET,
        HttpEntity.EMPTY,
        new ParameterizedTypeReference<List<ScheduleDay>>() {}
    );
    return scheduleDays.getBody();
  }

  @Override
  public ExpandedSchedule expandedSchedule(LocalDate day) {
    String apiUrl = baseUrl + "/schedule/day/{day}";
    return restTemplate.getForObject(apiUrl, ExpandedSchedule.class, Map.of("day", day));
  }
}
