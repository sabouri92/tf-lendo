package com.mrsabouri.tennisfollow.orderofplayclient.model;

public class Team {
  private String player1;
  private String player2;

  public String getPlayer1() {
    return player1;
  }

  public String getPlayer2() {
    return player2;
  }
}
