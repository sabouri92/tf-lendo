package com.mrsabouri.tennisfollow.orderofplayclient.model;

import java.time.Instant;

public class Match {
  private RecognizedBy recognizedBy;
  private Long matchId;
  private Team team1;
  private Team team2;
  private Team alternativeTeam1;
  private Team alternativeTeam2;
  private MatchType matchType;
  private Instant notBefore;

  public RecognizedBy getRecognizedBy() {
    return recognizedBy;
  }

  public Long getMatchId() {
    return matchId;
  }

  public Team getTeam1() {
    return team1;
  }

  public Team getTeam2() {
    return team2;
  }

  public Team getAlternativeTeam1() {
    return alternativeTeam1;
  }

  public Team getAlternativeTeam2() {
    return alternativeTeam2;
  }

  public MatchType getMatchType() {
    return matchType;
  }

  public Instant getNotBefore() {
    return notBefore;
  }
}
