package com.mrsabouri.tennisfollow.orderofplayclient;

import com.mrsabouri.tennisfollow.orderofplayclient.model.ExpandedSchedule;
import com.mrsabouri.tennisfollow.orderofplayclient.model.ScheduleDay;

import java.time.LocalDate;
import java.util.List;

public interface OrderOfPlayClient {
  List<ScheduleDay> scheduleDays();

  ExpandedSchedule expandedSchedule(LocalDate day);
}
