package com.mrsabouri.tennisfollow.orderofplayclient.model;

public enum MatchType {
  SINGLES,
  DOUBLES
}
