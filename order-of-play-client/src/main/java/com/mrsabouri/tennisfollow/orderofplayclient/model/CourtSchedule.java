package com.mrsabouri.tennisfollow.orderofplayclient.model;

import java.util.List;

public class CourtSchedule {
  private long tournamentId;
  private String courtName;
  private List<Match> matches;

  public long getTournamentId() {
    return tournamentId;
  }

  public String getCourtName() {
    return courtName;
  }

  public List<Match> getMatches() {
    return matches;
  }
}
