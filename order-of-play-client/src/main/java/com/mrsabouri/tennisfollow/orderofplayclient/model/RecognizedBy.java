package com.mrsabouri.tennisfollow.orderofplayclient.model;

public enum RecognizedBy {
  MATCH_ID,
  PLAYER_NAMES
}
