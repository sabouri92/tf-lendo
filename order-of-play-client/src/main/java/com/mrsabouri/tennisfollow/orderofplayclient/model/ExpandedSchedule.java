package com.mrsabouri.tennisfollow.orderofplayclient.model;

import java.time.LocalDate;
import java.util.List;

public class ExpandedSchedule {
  private LocalDate day;
  private List<CourtSchedule> courtSchedules;

  public LocalDate getDay() {
    return day;
  }

  public List<CourtSchedule> getCourtSchedules() {
    return courtSchedules;
  }
}
