package com.mrsabouri.tennisfollow.orderofplayclient;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mrsabouri.tennisfollow.orderofplayclient.model.ScheduleDay;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import java.time.LocalDate;
import java.util.List;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ScheduleDayApiTests extends ClientTestContext {
  @Autowired
  private ObjectMapper objectMapper;

  @Autowired
  private OrderOfPlayClient orderOfPlayClient;

  @Test
  public void scheduleDaysApi() throws JsonProcessingException {
    // Setup
    List<ScheduleDay> mockResponse = List.of(
        scheduleDay(LocalDate.now()),
        scheduleDay(LocalDate.now().minusDays(1)),
        scheduleDay(LocalDate.now().minusDays(5))
    );

    String json = objectMapper.writeValueAsString(mockResponse);

    stubFor(get(urlEqualTo("/schedule/days"))
        .willReturn(aResponse()
            .withStatus(HttpStatus.OK.value())
            .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
            .withBody(json)
        ));

    // Call
    List<ScheduleDay> actualResponse = orderOfPlayClient.scheduleDays();

    // Verify
    assertNotNull(actualResponse);
    assertEquals(3, actualResponse.size());
    for (int i = 0;i < 3;i++) {
      assertEquals(mockResponse.get(i).getDay(), actualResponse.get(i).getDay());
    }
  }

  private ScheduleDay scheduleDay(LocalDate date) {
    return new ScheduleDay() {
      @Override
      public LocalDate getDay() {
        return date;
      }
    };
  }
}
