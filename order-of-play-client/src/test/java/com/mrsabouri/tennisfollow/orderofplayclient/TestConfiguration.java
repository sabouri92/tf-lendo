package com.mrsabouri.tennisfollow.orderofplayclient;

import com.mrsabouri.tennisfollow.orderofplayclient.impl.OrderOfPlayClientImpl;
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJson;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import static com.mrsabouri.tennisfollow.orderofplayclient.ClientTestContext.WIREMOCK_PORT;

@Configuration
@AutoConfigureJson
public class TestConfiguration {
  @Bean
  public RestTemplate restTemplate() {
    return new RestTemplate();
  }

  @Bean
  public OrderOfPlayClient orderOfPlayClient(RestTemplate restTemplate) {
    return new OrderOfPlayClientImpl(restTemplate, "http://localhost:" + WIREMOCK_PORT);
  }
}
