package com.mrsabouri.tennisfollow.orderofplayclient;

import com.github.tomakehurst.wiremock.junit.WireMockRule;
import org.junit.Rule;
import org.junit.runner.RunWith;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;

@Import(TestConfiguration.class)
@RunWith(SpringRunner.class)
public class ClientTestContext {
  public static final int WIREMOCK_PORT = 55534;

  @Rule
  public WireMockRule wireMockRule = new WireMockRule(wireMockConfig().port(WIREMOCK_PORT));
}
