package com.mrsabouri.lib.jsouputils;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.web.client.RestTemplate;

public class DocumentLoader {
  private RestTemplate restTemplate;

  public DocumentLoader(RestTemplate restTemplate) {
    this.restTemplate = restTemplate;
  }

  public Document getByUrl(String url) {
    String content = restTemplate.getForObject(url, String.class);

    //noinspection ConstantConditions
    return Jsoup.parse(content);
  }
}
