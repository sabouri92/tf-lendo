package com.mrsabouri.lib.jsouputils;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class XPathResolver {
  public static Elements getByXPath(Element element, String xpath) {
    Elements res = new Elements();

    if (xpath.startsWith("/")) {
      xpath = xpath.substring(1);
    }

    String tag = xpath;
    if(tag.contains("/")) {
      tag = tag.substring(0, tag.indexOf('/'));
    }

    int idx = -1;
    if(tag.contains("[")) {
      idx = Integer.parseInt(tag.substring(tag.indexOf('[')+1, tag.indexOf(']')));
      tag = tag.substring(0, tag.indexOf('['));
    }

    if(xpath.contains("/")) {
      xpath = xpath.substring(xpath.indexOf('/'));
    }
    else {
      xpath = "";
    }

    for(Element child : element.children()) {
      if(child.tagName().equals(tag)) {
        idx--;
        if(idx <= 0) {
          if(xpath.length() > 0) {
            res.addAll(getByXPath(child, xpath));
          }
          else {
            res.add(child);
          }
        }

        if(idx == 0)
          break;
      }
    }

    return res;
  }

}
