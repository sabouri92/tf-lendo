package com.mrsabouri.lib.jsouputils;

import org.apache.commons.lang3.Validate;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.Elements;

public class ElementResolver {
  public static String getText(Element element) {
    return sanitize(element.text());
  }

  public static String getText(TextNode textNode) {
    return sanitize(textNode.getWholeText());
  }

  private static String sanitize(String str) {
    return str.replaceAll("\u00a0", " ").trim();
  }

  public static String resolveSingleElementText(String tagName, Elements elements) {
    Element element = resolveSingleElement(tagName, elements);
    return getText(element);
  }

  public static String resolveSingleElementAttribute(String tagName, String attribute, Elements elements) {
    Element element = resolveSingleElement(tagName, elements);
    return element.attr(attribute);
  }

  public static Element resolveSingleElement(String tagName, Elements elements) {
    Validate.isTrue(elements.size() == 1);

    Element element = elements.first();

    Validate.isTrue(tagName.equals(element.tagName()));

    return element;
  }
}
